// Main.cpp : Defines the entry point for the console application.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../include/NMMediator.h"

void test_insert_select_tasks()
{
    char xmlProps[1024]={0};

    const char *format =
"<taskprops>\n"
    "<specified>%d</specified>\n"
    "<interval>5</interval>\n"
    "<frequency>5</frequency>\n"
    "<command>access-list 102 permit ip</command>\n"
    "<desc>Trigger per 1000ms</desc>\n"
"</taskprops>\0";

    for (int i=1; i<51; i=i+5)
    {
        sprintf(xmlProps, format, i);
        NM_insertTask(xmlProps);
    }
}

void test_insert_select_rules()
{
    char *xmlProps = NULL;
    char *format = 
    "<ruleprops>\n"
	"<syntax>packet.data[12-13]='0806' end</syntax>\n"
        "<desc></desc>\n"
        "<enabled></enabled>\n"
        "<timeout>100</timeout>\n"
        "<command>\n"
		"    <lock>LOCK=> SRCMAC=${SRCMAC}, DESMAC=${DESMAC}, SRCIP=${SRCIP}, DESIP=${DESIP}, SRCPORT=${SRCPORT}, DESPORT=${DESPORT}</lock>\n"
		"    <unlock>UNLOCK=> SRCMAC=${SRCMAC}, DESMAC=${DESMAC}, SRCIP=${SRCIP}, DESIP=${DESIP}, SRCPORT=${SRCPORT}, DESPORT=${DESPORT}</unlock>\n"
        "</command>\n"
    "</ruleprops>";

    char *ruleMark = NM_insertRule(format);
    int count;
    char **list = NM_getRuleList("select * from RuleTable", &count);
    for (int i=0; i<count; i++)
    {
        char *fieldValue = list[i];
        printf("%s\n", fieldValue);
        
        xmlProps = NM_getRuleProps(fieldValue);
        printf("\n------------>>> \n%s\n", xmlProps);
    }

    NM_freeList(list, count);
    NM_free(xmlProps);
}

void test_stats_case()
{
//	char *syntax = "packet.data[12-13]='0806' end";
	char *syntax = "udp.data[0]=2 or udp.data[19]=2 ## tcp.data[0]=2";
	char md5Chars[64];
	NM_insertStats(syntax);
/*
	char md5Chars2[64];
	strcpy(md5Chars2, NM_insertStats("packet.data[12-13]='0800' end"));
	_sleep(6000);
	int localBytes = NM_getLocalIOBytes(md5Chars);
	printf("===========>md5Chars = [%d] bytes\n", localBytes);

	localBytes = NM_getLocalIOBytes(md5Chars2);
	printf("===========>md5Chars2 = [%d] bytes\n", localBytes);
*/
//	NM_removeStats(md5Chars);
//	NM_removeStats(md5Chars2);
}

void test_telent_config()
{
	char *username, *password;
	int port;
	char *xmlProps = 
"<telnet port=\"23\" prompt=\"[admin@MikroTik]\">\n"
"	<negotiation keyword=\"login:\" value=\"admin\"/>\n"
"	<negotiation keyword=\"password:\"    value=\"\"/>\n"
//"	<negotiation keyword=\"router&gt;\"   value=\"en\"/>\n"
"	<negotiation keyword=\"Please press &quot;Enter&quot; to continue\" value=\"\" />\n"
"</telnet>";


	NM_setTelnetConfig(xmlProps);
	xmlProps = NM_getTelnetConfig();

	NM_free(xmlProps);
}

void test_snmp_config()
{
	int version;
	char *mibspath, *security, *password;
	char *ip;

	//设置远程server ip
	NM_setRemoteIp("127.0.0.1");

	//取得远程server ip
	ip = NM_getRemoteIp();

	//配置snmp 参数
	NM_setSnmpConfig(1, "", "public", "");
	//取得snmp 参数
	NM_getSnmpConfig(&version, &mibspath, &security, &password);

	//连接snmp server
	NM_openSnmpServer();

	//向snmp server发送数据
	char v[3] = "yy";
	int ret;
	ret = NM_sendSnmpObjectMeta(".1.3.6.1.2.1.1.6.0", 0x04, v, 2);

	//从snmp server取数据
	int type, len;
	char *value = NULL;
	ret = NM_getSnmpObjectMeta(".1.3.6.1.2.1.1.6.0", &type, &value, &len);

	//关闭snmp server
	NM_closeSnmpServer();

	//释放内存
	NM_free(value);
	NM_free(mibspath);
	NM_free(security);
	NM_free(password);
	NM_free(ip);
}

void test_send_telnet_command()
{
//	for ( int i=0; i<10; i++) {
//		int ret = NM_sendCommandToDevice(0x02, "ip address print");
//		printf("result=%d\n", ret);

		int ret = NM_sendCommandToDevice(0x01, "ip address print");
		printf("result=%d\n", ret);
//		_sleep(2000);
//	}
}

void STDCALL triggerEvent(EventObject eventObj)
{
//	printf("Event from=%d\n", eventObj.comefrom);
//	printf("Event Mark String=%s\n", eventObj.md5Mark);
//	printf("Event Desc String=%s\n", eventObj.desc);
}

int main(int argc, char* argv[])
{
	int count;
	char **dev_name_list = NM_lookupDevices(&count);
	for (int i=0; i<count; i++)
	{
		printf("%d, %s\n", i+1, dev_name_list[i]);
	}

	int select;
	printf("选择网卡号: ");
	scanf("%d", &select);
	if ( select>count || select<1 )
	{
		printf("选择网卡错误\n");
		return -1;
	}

	//向数据库添加记录，如果存在，就跳过
    int result = NM_openDatabase(NULL);

	NM_setRemoteIp("172.28.10.225");
	test_telent_config();
//	test_snmp_config();
	NM_executeSQL("delete from TaskTable");
	NM_executeSQL("delete from StatsTable");
	NM_executeSQL("delete from RuleTable");

//    test_insert_select_tasks();
	NM_setEventCallback( triggerEvent );
	NM_setSSHConfig("admin", "", 22, "");
	test_insert_select_rules();
//	test_stats_case();
	NM_startMonitor(dev_name_list[select-1], 1024*10, true, 100, NULL);
	
//	NM_setMapEnabled( true );

    while ( 1 )
    {
        char input = getchar();
        if ( input=='q' )
        {
            NM_stopMonitor();
			NM_closeDatabase();
            break;
        }
		else if ( input=='m' )
		{
			char **list;
			int count;
			list = NM_getIpsMapTable(&count);
			for (int i=0; i<count; i++) {
				char *ip = list[i];
				char *mac = NM_getMacByIp( ip );
				NM_free( mac );
			}

			NM_freeList(list, count);
		}
		else if ( input=='k' )
		{
			test_stats_case();
		}
		else if (input=='s')
		{
			test_send_telnet_command();
		}
    }

	NM_freeList(dev_name_list, count);
    return NM_closeDatabase();
}