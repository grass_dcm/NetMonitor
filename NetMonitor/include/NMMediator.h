/**
 * NMMediator Library
 * 
 * Copyright (C) 2007 keesion <keesion@gmail.com>
 */

#ifndef __NM_MEDIATOR_H
#define __NM_MEDIATOR_H

/** 
 * @addtogroup api NetMediator API
 * @{
 */

/** IP数据包的信息 */
typedef struct _PACKET_DATA_ {
	char      mac[32];    /** MAC地址 */
	char      ip[32];     /** IP地址 */
	int       port;       /** 端口号 */
} PacketData;

typedef struct _RULE_IP_PACKET_ {
	unsigned char   type;      /** IP 类型 (TCP=6 or UDP=17) */
	PacketData      src;       /** 进入的数据包 */
	PacketData      des;       /** 出去的数据包*/
} RuleIPPacket;

/** @typedef EventObject    */
/** @struct _EVENT_OBJECT_  */
typedef struct _EVENT_OBJECT_ {
    /**
	 * 事件类别<br>
     *      1). comefrom=0x01 来自规则自动触发事件<br>
     *      2). comefrom=0x02 来自任务触发事件
     */
    int         comefrom;

    /**
     * 规则或任务的MD5标识串<br>
     *      1). if comefrom=0x01 md5Mark为规则的标识串<br>
     *      2). if comefrom=0x02 md5Mark为任务的标识串
     */
    const char  *md5Mark;

    /**
     * 事件的描述，做为保留参数，不见得会用到。
     */
    const char  *desc;

	/**
	 * IP数据包信息
	 */
	RuleIPPacket packet;

} EventObject;

#define STDCALL __stdcall
#ifdef __cplusplus
extern "C" {
#endif

/**
 * 事件的回调函数接口定义
 */
typedef void (STDCALL *EventCallback)(EventObject eventObj);

/** 函数执行成功状态 */
#define NMS_SUCCESS  0
/** 函数执行失败状态 */
#define NMS_FAILED  -1

/**
 * 加入一条规则
 * @param xmlProps xml结构的规则属性信息
 * @return 对传入数据进行MD5加密后所得的16进制字符串，做为每条规则的唯一标识<br>
 *         注意：本函数的返回指针不需要释放 
 *         返回NULL的时候说明执行失败
 */
char *  STDCALL NM_insertRule(const char *xmlProps);
/**
 * 更新一条规则
 * @param ruleMark 由insertRule返回的MD5标识串
 * @param xmlProps xml结构的规则属性信息
 * @return  0 函数执行成功<br>
 *         -1 操作失败
 */
int     STDCALL NM_updateRule(const char *ruleMark, const char *xmlProps);
/**
 * 删除一条规则
 * @param ruleMark 由insertRule返回的MD5标识串
 * @return  0 函数执行成功<br>
 *         -1 操作失败
 */
int     STDCALL NM_removeRule(const char *ruleMark);
/**
 * 根据SQL查询语句获取规则标识列表
 * @param sql 标准SQL查询语句。
 * @param count  返回规则的数量
 * @return 由规则的标识串组成的String数组<br>
 *         用完后，用NM_freeList释放内存
 */
char**  STDCALL NM_getRuleList(const char *sql, int *count);
/**
 * 获得指定规则的属性信息
 * @param ruleMark   由insertRule返回的MD5标识串
 * @return  xml结构的规则属性信息
 */
char *  STDCALL NM_getRuleProps(const char *ruleMark);

/**
 * 加入一项任务
 * @param xmlProps xml结构的任务属性信息
 * @return 对传入数据进行MD5加密后所得的16进制字符串，做为每项任务的唯一标识
 *         注意：本函数的返回指针不需要释放
 *         返回NULL的时候说明执行失败
 */
char *  STDCALL NM_insertTask(const char *xmlProps);
/**
 * @param taskMark 由insertTask返回的md5标识串
 * @param xmlProps xml结构的任务属性信息
 * @return  0 函数执行成功<br>
 *         -1 操作失败
 */
int     STDCALL NM_updateTask(const char *taskMark, const char *xmlProps);
/**
 * 删除一项任务
 * @param taskMark 由insertTask返回的MD5标识串
 * @return  0 函数执行成功<br>
 *         -1 操作失败
 */
int     STDCALL NM_removeTask(const char *taskMark);
/**
 * 根据SQL查询语句获取任务的标识列表
 * @param sql 标准SQL查询语句。
 * @param count  返回规则的数量
 * @return 由任务的标识串组成的String数组
 *         用完后，用NM_freeList释放内存
 */
char**  STDCALL NM_getTaskList(const char *sql, int *count);
/**
 * 获得指定任务的属性信息
 * @param taskMark   由insertTask返回的MD5标识串
 * @return  xml结构的任务属性信息
 */
char *  STDCALL NM_getTaskProps(const char *taskMark);

/**
 * 加入一条统计规则
 * @param syntax 统计规则的语法
 * @return 对传入数据进行MD5加密后所得的16进制字符串
 *         注意：本函数的返回指针不需要释放
 *         返回NULL的时候说明执行失败
 */
char *  STDCALL NM_insertStats(const char *syntax);
/**
 * 更新一条统计规则
 * @param statsMark 由insertStats返回的md5标识串
 * @param syntax 统计规则的语法
 * @return  0 函数执行成功<br>
 *         -1 操作失败
 */
int     STDCALL NM_updateStats(const char *statsMark, const char *syntax);
/**
 * 删除一条统计规则
 * @param statsMark 由insertStats返回的MD5标识串
 * @return  0 函数执行成功<br>
 *         -1 操作失败
 */
int     STDCALL NM_removeStats(const char *statsMark);
/**
 * 根据SQL查询语句获取统计规则的标识列表
 * @param sql 标准SQL查询语句。
 * @param count  返回统计规则的数量
 * @return 由统计规则的标识串组成的String数组
 *         用完后，用NM_freeList释放内存
 */
char**  STDCALL NM_getStatsList(const char *sql, int *count);
/**
 * 获得指定统计规则的属性信息
 * @param statsMark   由insertStats返回的MD5标识串
 * @return  统计规则的语法
 */
char *  STDCALL NM_getStatsProps(const char *statsMark);

/**
 * 设置用什么协议发送系统的自动命令
 * @param protocol 发送的协议<br>
 *                 1). protocol=0x01, 用SSH协议发送<br>
 *                 2). protocol=0x02, 用Telnet协议发送
 * @return  0 函数执行成功<br>
 *         -1 操作失败
 */
int     STDCALL NM_setAutoCommandProtocol(int protocol);
/**
 * 给设备发送一条指令
 * @param protocol 发送的协议<br>
 *                 1). protocol=0x01, 用SSH协议发送<br>
 *                 2). protocol=0x02, 用Telnet协议发送
 * @param commandString 设备支持的命令字符串
 * @return  0 函数执行成功<br>
 *         -1 操作失败
 */
int     STDCALL NM_sendCommandToDevice(int protocol, const char *commandString);
/**
 * 读取本地的系统网络数据流量
 * @param statsMark 统计规则的md5标识串
 * @return 数据流量，单位为byte
 */
long    STDCALL NM_getLocalIOBytes(const char *statsMark);
/**
 * 读取本地的系统网络数据包个数
 * @param statsMark 统计规则的md5标识串
 * @return 数据包个数
 */
long    STDCALL NM_getLocalIOPackets(const char *statsMark);
/**
 * 设置警告或日志事件对象
 * @param EventCallback 回调函数原型
 * @return NULL(void)
 */
void    STDCALL NM_setEventCallback(EventCallback fireEventObj);
/**
 * 启动网络数据监控
 * @param device 网卡的名称(friendly name)
 * @param snaplen 要监控的数据包总长度, 满足长度后停止监控
 * @param promiscuous 是否要混合监听模式
 * @param timeout 读取网络包的超时时间
 * @param filter  过滤语法
 * @return 0 函数执行成功
 */
int     STDCALL NM_startMonitor(const char* device, int snaplen, 
                                bool promiscuous, int timeout, 
                                const char *filter);
/**
 * 停止网络数据监控
 * @return 0 函数执行成功
 */
int     STDCALL NM_stopMonitor();

/**
 * 分配内存
 * @param size 要分配的内存的大小
 */
void*   STDCALL NM_malloc(int size);
/**
 * 释放内存
 * @param pObject 要释要的内存地址
 */
void    STDCALL NM_free(void *pObject);

/**
 * 释放列表内存
 * @param ppObject 列表首地址
 * @param  count    字串列表中字串的数量
 */
void    STDCALL NM_freeList(char **ppObject, int count);

/**
 * 打开数据库
 * @param fileName 数据文件名，为NULL时，以默认文件名(NMConfig.db)打开
 * @return 0 函数执行成功<br>
 *        -1 操作失败
 */
int     STDCALL NM_openDatabase(char *fileName);
/**
 * 关闭数据库
 * @return 0 函数执行成功<br>
 *        -1 操作失败<br>
 *        -2 数据库尚未打开
 */
int     STDCALL NM_closeDatabase();
/**
 * 判断是否存在记录
 * @param sql 查询的SQL语句
 * @return true  <=> 存在<br>
 *         false <=> 不存在
 */
bool    STDCALL NM_hasRecordset(const char *sql);
/**
 * 执行一条sql语句
 * @param sql 要执行的sql语句
 * @return 0 函数执行成功<br>
 *        -1 操作失败
 */
int     STDCALL NM_executeSQL(const char *sql);
/**
 * 返回最后一条错误信息
 */
const char*   STDCALL NM_getLastError();

/**
 * 返回所有网卡的名称
 * @param count 网卡数量
 * @return 网卡名称列表
 */
char **  STDCALL NM_lookupDevices(int *count);

/**
 * 设置远程设备ip
 * @param ip 远程设备ip
 * @return 0 函数执行成功<br>
 *        -1 操作失败
 */
int      STDCALL NM_setRemoteIp(char *ip);
/**
 * 获取远程设备ip
 * @return 远程设备ip
 */
char *   STDCALL NM_getRemoteIp();
/**
 * 设置Telnet配置信息
 * @param xmlProps Telnet的XML配置信息
 * @return 0 函数执行成功<br>
 *        -1 操作失败
 */
int      STDCALL NM_setTelnetConfig(const char *xmlProps);
/**
 * 获取Telnet配置信息
 * @return 返回Telnet的XML配置信息
 */
char *   STDCALL NM_getTelnetConfig();

/**
 * 设置SNMP配置信息
 * @param version 版本号
 * @param mibspath mib存放路径
 * @param security 如果是V1就是PUBLIC，如果是V3就是用户名
 * @param password v3, 用户密码
 * @return 0 函数执行成功<br>
 *        -1 操作失败
 */
int      STDCALL NM_setSnmpConfig(int version, char *mibspath, char *security, char *password);
/**
 * 获取SNMP配置信息
 * @param version 版本号
 * @param mibspath mib存放路径
 * @param security 如果是V1就是PUBLIC，如果是V3就是用户名
 * @param password v3, 用户密码
 * @return 0 函数执行成功<br>
 *        -1 操作失败
 */
int      STDCALL NM_getSnmpConfig(int *version, char **mibspath, char **security, char **password);
/**
 * 与SNMP Server连接
 * @return 0 函数执行成功<br>
 *        -1 操作失败
 */
int      STDCALL NM_openSnmpServer();

/**
 * 根据oids设置路由中的对应值
 * @param oids  MIB OID,形式".1.3.6.1.2.1.1.5.1.0"
 * @param type  值的类型,比如0x04为字符串类型
 * @param value 值的缓冲区指针
 * @param len   值的缓冲区指针的长度
 * @return 0 函数执行成功<br>
 *        -1 操作失败
 */ 
int      STDCALL NM_sendSnmpObjectMeta(char *oids, int type, char *value, int len);
/**
 * 读取路由中指定OID的值
 * @param oids  MIB OID,形式".1.3.6.1.2.1.1.5.1.0"
 * @param type  值的类型,比如0x04为字符串类型
 * @param ppValue 存放返回值的缓冲区指针
 * @param len   存放返回值的缓冲区指针的长度
 * @return 0 函数执行成功<br>
 *        -1 操作失败
 */
int      STDCALL NM_getSnmpObjectMeta(char *oids, int *type, char **ppValue, int *len);
/**
 * 关闭与SNMP Server的连接
 * @return 0 函数执行成功<br>
 *        -1 操作失败
 */
int      STDCALL NM_closeSnmpServer();

/**
 * 设置SSH配置信息
 * @param username 用户名
 * @param password 密码
 * @param port SSH Server端口号
 * @param key SSH Private Key
 * @return 0 函数执行成功<br>
 *        -1 操作失败
 */
int      STDCALL NM_setSSHConfig(char *username, char *password, int port, char *key);
/**
 * 获取SSH配置信息
 * @param username 用户名
 * @param password 密码
 * @param port SSH Server端口号
 * @param key SSH Private Key
 * @return 0 函数执行成功<br>
 *        -1 操作失败
 */
int      STDCALL NM_getSSHConfig(char **username, char **password, int *port, char **key);
/**
 * 设置是否对数据包进行 ip<=>mac 的提取
 * @param bEnabled true   开始提取<br>
 *                 false  停止提取
 * @return 0 函数执行成功<br>
 *        -1 操作失败
 */
int      STDCALL NM_setMapEnabled(bool bEnabled);
/**
 * 获取通过Device上的IP映射表
 * @return Device上的IP映射表
 */
char **  STDCALL NM_getIpsMapTable(int *count);
/**
 * 根据ip取得对应的mac
 * @param ip ip字串，由getIpsMapTable返回的列表中挑选
 * @return　mac
 */
char *   STDCALL NM_getMacByIp(char *ip);
/**
 * 清除ip映射表，释放占用的内存
 * @return 0 函数执行成功<br>
 *        -1 操作失败
 */
int      STDCALL NM_clearIpsMapTable();

/** @} */

#ifdef __cplusplus
}
#endif
#endif



