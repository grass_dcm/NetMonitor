#ifndef __UTIL_H
#define __UTIL_H

#pragma warning( disable : 4996 )
#pragma warning( disable : 4267 )

#define STR2INT(value) (value==NULL) ? 0 : atoi(value)
#define CHECK_NULL(value) if (!value) value = strdup("");
#define FREE_AND_NULL(value) { if (value) { NM_free(value); value=NULL; } }
#define CHK_NULL_S(value) (value==NULL ? "" : value)

#define DEFAULT_DB_NAME        "NMConfig.db";
#define MD5_MARK_FIELD_NAME    "md5Chars"
#define PROPS_FIELD_NAME       "propsText"

#define SELECT_FROM_TABLE(tableName) "select " MD5_MARK_FIELD_NAME " from "#tableName

extern EventCallback fireLoggerEventCallbak;
extern TaskScheduler taskSchedulerObject;
extern void setErrorMessage(const char *format, ...);

// IP MAP TABLE 
extern char** map_get_table(int* count);
extern char*  map_get_mac_by_ip(char* ip);
extern void   map_set_enable(bool enable);
extern void   map_clear_table();

extern char auto_command_protocol;

#endif //end #ifndef __UTIL_H