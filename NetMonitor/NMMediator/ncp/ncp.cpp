// ncp.cpp : Defines the entry point for the console application.
//
#include <conio.h>
#include "ncp_cap.h"
#include "ncp_rule.h"
#include "ncp_parse.h"

#include "pcap.h"
void list_devs()
{
	pcap_if_t *alldevs;
	pcap_if_t *d;
	
	int i=0;
	
	char errbuf[PCAP_ERRBUF_SIZE];
	

	if (pcap_findalldevs(&alldevs, errbuf) == -1)
	{
		fprintf(stderr,"Error in pcap_findalldevs: %s\n", errbuf);
		exit(1);
	}
	
	
	for(d=alldevs; d; d=d->next)
	{
		printf("%d. %s", ++i, d->name);
		if (d->description)
			printf(" (%s)\n", d->description);
		else
			printf(" (No description available)\n");
	}
}

int main(int argc, char* argv[])
{
	def_session_struct session_struct;
	session_struct.device_name    = "\\Device\\NPF_{5C2BFDEB-1C60-45FF-BF46-3A9842DB4C0F}";
	session_struct.flags          = 1;
	session_struct.timeout        = 1000;
	session_struct.filter_text    = NULL;
	session_struct.max_task_count = 5;
	session_struct.log_io_type    = enum_log_io_type_std;
	session_struct.log_file_name  = NULL;
	
	ncp_cap_start_cap(&session_struct);
	getch();
	ncp_cap_stop_cap();
	return 0;
}

