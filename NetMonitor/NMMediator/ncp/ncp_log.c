#include "ncp_log.h"
#include <time.h>

FILE* g_log_io = 0;

FILE* log_init_io(def_enum_log_io_type log_io_type, char* path)
{
	switch(log_io_type)
	{
	case enum_log_io_type_std:
		g_log_io = stdout;
		break;
	case enum_log_io_type_file:
		if(NULL != path)
		{
			g_log_io = fopen(path, "ac");
		}
		else
		{
			g_log_io = fopen(".\\log.txt", "ac");
		}
		break;
	default:
		g_log_io = 0;
		break;
	}

	return g_log_io;
}

void log_flush_io()
{
	fflush(g_log_io);
}

void log_close_io()
{
	fflush(g_log_io);
	if(stdout != g_log_io)
	{
		fclose(g_log_io);
	}
}

void log_stamp()
{
	char dbuffer [9];
	char tbuffer [9];
	_strdate( dbuffer );
	_strtime( tbuffer );
	fprintf(g_log_io, "%s %s\n", dbuffer, tbuffer);		

}
