#ifndef _H_NCP_LOG_
#define _H_NCP_LOG_

#include <stdio.h>
#include <stdlib.h>

#define LOG_LEVEL_ONE		5
#define LOG_LEVEL_TWO		4
#define LOG_LEVEL_THR		3
#define LOG_LEVEL_FOR		2
#define LOG_LEVEL_FIV		1

#define LOG_CUR_LEVEL		LOG_LEVEL_ONE

#if LOG_LEVEL_ONE >= LOG_CUR_LEVEL
#define LOG_SESSION_START				"***********session start***********\n"
#define LOG_SESSION_END					"***********session end*************\n"
#define LOG_SESSION_OK					"***********session success*************\n"
#define LOG_ENABLE_MACIP_TABLE			"enable mac ip table\n"
#define LOG_DISABLE_MACIP_TABLE			"disable mac ip table\n"
#define LOG_CAP_FILTER                  "set winpcap filter(%s)\n"
#endif

#if LOG_LEVEL_ONE >= LOG_CUR_LEVEL
#define LOG_MALLOC_ERROR				"malloc memery error\n"
#define LOG_CAP_INIT_FAILD				"!!!!!wincap init faild\n"
#define LOG_CAP_LINK_FAILD				"!!!!!wincao link faild\n"
#define LOG_CAP_COMPILE_ERROR			"Unable to compile the packet filter. Check the syntax.\n"
#define LOG_CAP_SET_FILTER_ERROR		"Error setting the filter.\n"
#define LOG_RULE_CREATE_FAILD			"!!!!!create rule list faild\n"
#define LOG_THREAD_FAILD				"!!!!!create thread faild\n"
#define LOG_RULE_PARSE_ERROR            "!!!!!rule%d parse error\n"
#define LOG_STAT_PARSE_ERROR            "!!!!!stat%d parse error\n"
#define LOG_NULL_POINTER_ERROR			"!!!!!invaild pointer\n"
#define LOG_RULE_TEXT_ERROR				"!!!!!invaild syntax\n"
#endif

#if LOG_LEVEL_TWO >= LOG_CUR_LEVEL
#define LOG_CAP_INIT_OK					"wincap init success\n"
#define LOG_CAP_LINK_OK					"wincao link success\n"
#define LOG_RULE_CREATE_OK				"create rule list success\n"
#define LOG_THREAD_OK					"create thread success\n"
#endif

#if LOG_LEVEL_THR >= LOG_CUR_LEVEL
#define LOG_RULE_START					"start rule list\n"
#define LOG_RULE_PARSE					"parse rule%d\n"
#define LOG_RULE_END					"end rule list\n"
#define LOG_STAT_START					"start stat list\n"
#define LOG_STAT_PARSE					"parse stat%d\n"
#define LOG_STAT_END					"end stat list\n"
#define LOG_CAP_EXIT					"thread cap exit\n"
#define LOG_PARSE_EXIT					"thread parse exit\n"
#endif

#if LOG_LEVEL_FOR >= LOG_CUR_LEVEL
#define LOG_RULE_DWAR					"draw rule%d tree\n"
#define LOG_DRAW_NOT					"$not\n"
#define LOG_DRAW_AND					"$and\n"
#define LOG_DRAW_OR						"$or\n"
#define LOG_DRAW_N						"$\n"
#define LOG_DRAW_BRAKET_LEFT			"$(\n"
#define LOG_DRAW_BRAKET_RIGHT			"$)\n"

#define LOG_DD_PACKET_DATA_BYTE			"#packet.data[%d] == %d\n"
#define LOG_DD_PACKET_DATA_AREA			"#packet.data[%d-%d] ==\n"
#define LOG_DD_IP_SRC					"#ip.src == %d.%d.%d.%d\n"
#define LOG_DD_IP_SRC_AREA				"#ip.src == %d.%d.%d.%d-%d.%d.%d.%d\n"
#define LOG_DD_IP_DES					"#ip.des == %d.%d.%d.%d\n"
#define LOG_DD_IP_DES_AREA				"#ip.des == %d.%d.%d.%d-%d.%d.%d.%d\n"
#define LOG_DD_MAC_SRC					"#mac.src == %x:%x:%x:%x:%x:%x\n"
#define LOG_DD_MAC_SRC_AREA				"#mac.src == %x:%x:%x:%x:%x:%x-%x:%x:%x:%x:%x:%x\n"
#define LOG_DD_MAC_DES					"#mac.des == %x:%x:%x:%x:%x:%x\n"
#define LOG_DD_MAC_DES_AREA				"#mac.des == %x:%x:%x:%x:%x:%x-%x:%x:%x:%x:%x:%x\n"
#define LOG_DD_TCP_PORT_SRC				"#tcp.port.src == %d\n"
#define LOG_DD_TCP_PORT_SRC_AREA		"#tcp.port.src == %d-%d\n"
#define LOG_DD_TCP_PORT_DES				"#tcp.port.des == %d\n"
#define LOG_DD_TCP_PORT_DES_AREA		"#tcp.port.des == %d-%d\n"
#define LOG_DD_TCP_DATA_BYTE			"#tcp.data[%d] == %d\n"
#define LOG_DD_TCP_DATA_AREA			"#udp.data[%d-%d] ==\n"
#define LOG_DD_UDP_PORT_SRC				"#udp.port.src == %d\n"
#define LOG_DD_UDP_PORT_SRC_AREA		"#udp.port.src == %d-%d\n"
#define LOG_DD_UDP_PORT_DES				"#udp.port.des == %d\n"
#define LOG_DD_UDP_PORT_DES_AREA		"#udp.port.des == %d-%d\n"
#define LOG_DD_UDP_DATA_BYTE			"#udp.data[%d] == %d\n"
#define LOG_DD_UDP_DATA_AREA			"#udp.data[%d-%d] ==\n"
#define LOG_DD_DATA						"%02X  "
#endif

#if LOG_LEVEL_FIV >= LOG_CUR_LEVEL
#define LOG_MATCH_RULE					"match rule%d\n"
#define LOG_MATCH_RULE_TRUE				"match rule true\n"
#define LOG_MATCH_RULE_FALSE			"match rule false\n"
#define LOG_MATCH_STAT					"match stat%d\n"
#define LOG_MATCH_STAT_TRUE				"match stat true\n"
#define LOG_MATCH_STAT_COUNT			"match stat count=%d\n"
#define LOG_MATCH_STAT_FALSE			"match rule false\n"
#define LOG_DEAL_EXCUTE					"deal excute type=%d res=%d\n"

#define LOG_DRAW_PACKET_START			"================packet==================\n"
#define LOG_DRAW_PACKET_END				"================packet end==============\n"
#define LOG_DRAW_ETHER_PACKET			"$ether\n$type=%x\n$mac src=%02X:%02X:%02X:%02X:%02X:%02X des=%02X:%02X:%02X:%02X:%02X:%02X\n"
#define LOG_DRAW_IP_PACKET				"$ip\n$type=%d\n$ipaddr src=%d.%d.%d.%d des=%d.%d.%d.%d\n"
#define LOG_DRAW_TCP_PACKET				"$tcp\ntcp flags=%x\ntcp port src=%d, des=%d\n"
#define LOG_DRAW_UDP_PACKET				"$udp\nudp port src=%d, des=%d\n"	
#endif

extern FILE* g_log_io;

typedef enum
{
	enum_log_io_type_std,
	enum_log_io_type_file 
}def_enum_log_io_type;

FILE* log_init_io(def_enum_log_io_type log_io_type, char* path);
void log_flush_io();
void log_close_io();
void log_stamp();


#endif  _H_NCP_LOG_