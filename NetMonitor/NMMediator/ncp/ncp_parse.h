#ifndef _NCP_PARSE_H_
#define _NCP_PARSE_H_

#include "pcap.h"
extern "C"
{
	#include "grammar/grammar.h"
}

//===========================================================================
// ip type
//===========================================================================

#define IPPROTO_IP              0               /* dummy for IP */
#define IPPROTO_HOPOPTS         0               /* IPv6 hop-by-hop options */
#define IPPROTO_ICMP            1               /* control message protocol */
#define IPPROTO_IGMP            2               /* internet group management protocol */
#define IPPROTO_GGP             3               /* gateway^2 (deprecated) */
#define IPPROTO_IPV4            4               /* IPv4 */
#define IPPROTO_TCP             6               /* tcp */
#define IPPROTO_PUP             12              /* pup */
#define IPPROTO_UDP             17              /* user datagram protocol */
#define IPPROTO_IDP             22              /* xns idp */
#define IPPROTO_IPV6            41              /* IPv6 */
#define IPPROTO_ROUTING         43              /* IPv6 routing header */
#define IPPROTO_FRAGMENT        44              /* IPv6 fragmentation header */
#define IPPROTO_ESP             50              /* IPsec ESP header */
#define IPPROTO_AH              51              /* IPsec AH */
#define IPPROTO_ICMPV6          58              /* ICMPv6 */
#define IPPROTO_NONE            59              /* IPv6 no next header */
#define IPPROTO_DSTOPTS         60              /* IPv6 destination options */
#define IPPROTO_ND              77              /* UNOFFICIAL net disk proto */
#define IPPROTO_ICLFXBM         78
#define IPPROTO_RAW             255             /* raw IP packet */
#define IPPROTO_MAX             256

//===========================================================================
// ether type
//===========================================================================

#define	ETHERMTU	        1500


#ifndef ETHERTYPE_LEN
#define ETHERTYPE_LEN       2
#endif

#ifndef ETHERTYPE_GRE_ISO
#define ETHERTYPE_GRE_ISO       0x00FE  /* not really an ethertype only used in GRE */
#endif
#ifndef ETHERTYPE_PUP
#define	ETHERTYPE_PUP			0x0200	/* PUP protocol */
#endif
#ifndef ETHERTYPE_IP
#define	ETHERTYPE_IP			0x0800	/* IP protocol */
#endif
#ifndef ETHERTYPE_ARP
#define ETHERTYPE_ARP			0x0806	/* Addr. resolution protocol */
#endif
#ifndef ETHERTYPE_REVARP
#define ETHERTYPE_REVARP		0x8035	/* reverse Addr. resolution protocol */
#endif
#ifndef ETHERTYPE_NS
#define ETHERTYPE_NS			0x0600
#endif
#ifndef	ETHERTYPE_SPRITE
#define	ETHERTYPE_SPRITE		0x0500
#endif
#ifndef ETHERTYPE_TRAIL
#define ETHERTYPE_TRAIL			0x1000
#endif
#ifndef	ETHERTYPE_MOPDL
#define	ETHERTYPE_MOPDL			0x6001
#endif
#ifndef	ETHERTYPE_MOPRC
#define	ETHERTYPE_MOPRC			0x6002
#endif
#ifndef	ETHERTYPE_DN
#define	ETHERTYPE_DN			0x6003
#endif
#ifndef	ETHERTYPE_LAT
#define	ETHERTYPE_LAT			0x6004
#endif
#ifndef ETHERTYPE_SCA
#define ETHERTYPE_SCA			0x6007
#endif
#ifndef	ETHERTYPE_LANBRIDGE
#define	ETHERTYPE_LANBRIDGE		0x8038
#endif
#ifndef	ETHERTYPE_DECDNS
#define	ETHERTYPE_DECDNS		0x803c
#endif
#ifndef	ETHERTYPE_DECDTS
#define	ETHERTYPE_DECDTS		0x803e
#endif
#ifndef	ETHERTYPE_VEXP
#define	ETHERTYPE_VEXP			0x805b
#endif
#ifndef	ETHERTYPE_VPROD
#define	ETHERTYPE_VPROD			0x805c
#endif
#ifndef ETHERTYPE_ATALK
#define ETHERTYPE_ATALK			0x809b
#endif
#ifndef ETHERTYPE_AARP
#define ETHERTYPE_AARP			0x80f3
#endif
#ifndef	ETHERTYPE_8021Q
#define	ETHERTYPE_8021Q			0x8100
#endif
#ifndef ETHERTYPE_IPX
#define ETHERTYPE_IPX			0x8137
#endif
#ifndef ETHERTYPE_IPV6
#define ETHERTYPE_IPV6			0x86dd
#endif
#ifndef ETHERTYPE_PPP
#define	ETHERTYPE_PPP			0x880b
#endif
#ifndef ETHERTYPE_SLOW
#define	ETHERTYPE_SLOW			0x8809
#endif
#ifndef	ETHERTYPE_MPLS
#define	ETHERTYPE_MPLS			0x8847
#endif
#ifndef	ETHERTYPE_MPLS_MULTI
#define	ETHERTYPE_MPLS_MULTI	0x8848
#endif
#ifndef ETHERTYPE_PPPOED
#define ETHERTYPE_PPPOED		0x8863
#endif
#ifndef ETHERTYPE_PPPOES
#define ETHERTYPE_PPPOES		0x8864
#endif
#ifndef ETHERTYPE_JUMBO
#define ETHERTYPE_JUMBO         0x8870
#endif
#ifndef ETHERTYPE_EAPOL
#define ETHERTYPE_EAPOL			0x888e
#endif
#ifndef	ETHERTYPE_LOOPBACK
#define	ETHERTYPE_LOOPBACK		0x9000
#endif
#ifndef	ETHERTYPE_VMAN
#define	ETHERTYPE_VMAN	        0x9100 /* Extreme VMAN Protocol */ 
#endif
#ifndef	ETHERTYPE_ISO
#define	ETHERTYPE_ISO           0xfefe  /* nonstandard - used in Cisco HDLC encapsulation */
#endif

//================================================================
// net struct
//================================================================

/* data */
typedef unsigned char* def_net_data;

/* ether header */
typedef struct
{
	def_macaddr     daddr;
	def_macaddr     saddr;
	unsigned short  type;	
}def_ether_header;

/* ip header */
typedef struct
{
	struct
	{
		char len	:4;
		char version:4;
	}ver_len;	
	u_char	tos;			// Type of service 
	u_short tlen;			// Total length 
	u_short identification; // Identification
	u_short flags_fo;		// Flags (3 bits) + Fragment offset (13 bits)
	u_char	ttl;			// Time to live
	u_char	proto;			// Protocol
	u_short crc;			// Header checksum
	def_ipaddr	saddr;		// Source address
	def_ipaddr	daddr;		// Destination address	
}def_ip_header;

/* udp header */
typedef struct
{
	u_short src_port;			// Source port
	u_short des_port;			// Destination port
	u_short len;			// Datagram length
	u_short crc;			// Checksum
}def_udp_header;

/* tcp header */
typedef struct 
{
	unsigned short	src_port;
	unsigned short	des_port;
	unsigned long	seq_num;
	unsigned long	ack_num;
	struct struct_length
	{
		unsigned char        :4;
		unsigned char length :4;
	}length;
	struct struct_flags
	{
		unsigned char    :1;
		unsigned char    :1;
		unsigned char urg:1;
		unsigned char ack:1;
		unsigned char psh:1;
		unsigned char ret:1;
		unsigned char syn:1;
		unsigned char fin:1;
	}flags;
	unsigned short	win;
	unsigned short	sum;
	unsigned short	urp;
}def_tcp_header;

/* packet struct */
typedef struct
{
	def_net_data      packet_data;
	def_count		  packet_data_len;
	def_ether_header  ether_header;
	def_net_data      ether_data;
	def_count		  ether_data_len;
	def_ip_header     ip_header;
	def_net_data      ip_data;
	def_count		  ip_data_len;
	def_udp_header    udp_header;
	def_net_data      udp_data;
	def_count		  udp_data_len;
	def_tcp_header    tcp_header;
	def_net_data      tcp_data;
	def_count		  tcp_data_len;
}def_packet_struct;

/*===================================================================*/
/* function                                                          */
/*===================================================================*/
/* parse packet */
void parse_packet(pcap_pkthdr *header, u_char *pkt_data);

/* get ip mac table*/
char** map_get_table(int* count);

/* get mac by ip */
char* map_get_mac_by_ip(char* ip);

/* enable ip mac table func */
void map_set_enable(bool enable);

/* clear map table */
void map_clear_table();

#endif  _NCP_PARSE_H_