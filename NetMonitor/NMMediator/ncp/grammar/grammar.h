#ifndef _GRAMMAR_H_
#define _GRAMMAR_H_

#include <stdlib.h>
#include <stdio.h>
#include <malloc.h>

/*=======================================================*/
/*type def                                               */
/*=======================================================*/
typedef unsigned short def_count;
typedef int            def_enum;
typedef int            def_bool;
#define def_true       1;
#define def_false      0;

/*=======================================================*/
/*type def for parse                                     */
/*=======================================================*/
/* ip addr */
typedef struct
{
	unsigned char char1; 
	unsigned char char2;
	unsigned char char3;
	unsigned char char4;
}def_ipaddr;

typedef struct
{
	def_ipaddr  ipaddr_from;
	def_ipaddr  ipaddr_to;
}def_ipaddr_area;

/* mac addr */
typedef struct
{
	unsigned char char1; 
	unsigned char char2;
	unsigned char char3;
	unsigned char char4;
	unsigned char char5;
	unsigned char char6;
}def_macaddr;

typedef struct
{
	def_macaddr macaddr_from;
	def_macaddr macaddr_to;
}def_macaddr_area;

/* number */

typedef int def_number;

typedef struct
{
	def_number number_from;
	def_number number_to;
}def_number_area;

/* hex string */

typedef struct
{
	def_count       count;
	unsigned char*  value;
}def_hex;

/* string */

typedef char* def_string;

/* value node */
typedef enum
{
	enum_value_type_ipaddr,
	enum_value_type_ipaddr_area,
	enum_value_type_macaddr,	
	enum_value_type_macaddr_area,	
	enum_value_type_number,
	enum_value_type_number_area,
	enum_value_type_hex,
	enum_value_type_string,
	enum_value_type_operation
}def_enum_value_type;

typedef struct
{
	def_enum_value_type value_type;
	union asd
	{
		def_ipaddr*       ipaddr;
		def_ipaddr_area*  ipaddr_area;
		def_macaddr*      macaddr;
		def_macaddr_area* macaddr_area;		
		def_number        number;
		def_number_area*  number_area;
		def_hex*          hex;
		def_string        str;
	}value_value;	
}def_value_node;

/* operation node*/
typedef enum
{
	enum_operation_type_op_not,
	enum_operation_type_op_and,
	enum_operation_type_op_or,
	enum_operation_type_op_bracket,

	enum_operation_type_packet_data_byte,
	enum_operation_type_packet_data_area,
	enum_operation_type_ip_src,
	enum_operation_type_ip_des,
	enum_operation_type_mac_src,
	enum_operation_type_mac_des,	
	enum_operation_type_tcp_port_src,
	enum_operation_type_tcp_port_des,
	enum_operation_type_tcp_data_byte,
	enum_operation_type_tcp_data_area,
	enum_operation_type_udp_port_src,
	enum_operation_type_udp_port_des,
	enum_operation_type_udp_data_byte,
	enum_operation_type_udp_data_area
}def_enum_operation_type;

struct def_node_tag;

typedef struct
{
	def_enum_operation_type  operation_type;
	def_count                operation_value_count;
	struct def_node_tag**    operation_values;	
}def_operation_node;

/* parse */
typedef enum
{
	enum_parse_type_program,
	enum_parse_type_expr,
	enum_parse_type_tern,
	enum_parse_type_end
}def_enum_parse_type;

/* node */
typedef enum
{
	enum_node_type_parse,
	enum_node_type_value,
	enum_node_type_operation
}def_enum_node_type;

typedef struct def_node_tag
{
	def_enum_node_type node_type;
	union 
	{
		def_value_node*     value;
		def_operation_node* operation;
	}node_value;
}def_node;


/*=======================================================*/
/*node function                                          */
/*=======================================================*/
/* create node */
def_node* grammar_create_node(def_enum_node_type node_type, def_enum enum_type, def_count parameter_count, ...);

/* free node */
def_bool grammar_free_node(def_node* node);

/* excute node */
def_bool grammar_excute_node(def_node* node);

/* deal excute */
extern def_bool grammar_deal_excute(def_operation_node* operation);

/* draw node */
def_bool grammar_draw_node(def_node* node);

/* deal draw */
def_bool grammar_deal_draw(def_operation_node* operation);

/*=======================================================*/
/*tree function                                          */
/*=======================================================*/
/* create tree */
def_node* grammar_create_tree(char* text);

/* excute tree */
def_bool grammar_excute_tree(def_node* node);

/* free tree */
def_bool grammar_free_tree(def_node* node);

/* draw tree */
def_bool grammar_draw_tree(def_node* node);

#endif _GRAMMAR_H_
