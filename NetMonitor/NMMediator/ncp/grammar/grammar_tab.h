typedef union {
	def_ipaddr*        val_ipaddr;
	def_ipaddr_area*   val_ipaddr_area;
	def_macaddr *      val_macaddr;
	def_macaddr_area*  val_macaddr_area;	
	def_number         val_number;
	def_number_area*   val_number_area;	
	def_hex*           val_hex;
	def_string         val_string;
	def_node*          val_node;
	char*              text;
	} YYSTYPE;
#define	IPADDR	258
#define	IPADDR_AREA	259
#define	MACADDR	260
#define	MACADDR_AREA	261
#define	NUMBER	262
#define	NUMBER_AREA	263
#define	HEX	264
#define	STRING	265
#define	END	266
#define	AND	267
#define	OR	268
#define	NOT	269
#define	SRC	270
#define	DES	271
#define	PORT	272
#define	DATA	273
#define	AREA	274
#define	MAC	275
#define	IP	276
#define	PACKET	277
#define	TCP	278
#define	UDP	279
#define	ETHER	280


extern YYSTYPE yylval;
