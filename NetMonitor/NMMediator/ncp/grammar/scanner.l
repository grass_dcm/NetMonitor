%{
#include "grammar.h"
#include "grammar_tab.h"

void yyerror(char *);
%}
BLOCK_STRING   ("'"([0-9a-f]+)"'");
BLOCK_NUMBER   ([0-9]+)
BLOCK_IP_BYTE  (([1-2])?([0-9])?[0-9])
BLOCK_IP_ADDR  {BLOCK_IP_BYTE}"."{BLOCK_IP_BYTE}"."{BLOCK_IP_BYTE}"."{BLOCK_IP_BYTE}
BLOCK_MAC_BYTE ([0-9A-Fa-f][0-9A-Fa-f])
BLOCK_MAC_ADDR {BLOCK_MAC_BYTE}":"{BLOCK_MAC_BYTE}":"{BLOCK_MAC_BYTE}":"{BLOCK_MAC_BYTE}":"{BLOCK_MAC_BYTE}":"{BLOCK_MAC_BYTE}
%%

"end"    return END;
"and"    return AND;
"or"     return OR;
"not"    return NOT;
"mac"    return MAC;
"ip"     return IP;
"packet" return PACKET;
"udp"    return UDP;
"tcp"    return TCP;
"src"    return SRC;
"des"    return DES;
"port"   return PORT;
"data"   return DATA;
{BLOCK_NUMBER} {	
	yylval.text = yytext;
	return NUMBER;
}
{BLOCK_NUMBER}"-"{BLOCK_NUMBER} {
	yylval.text = yytext;
	return NUMBER_AREA;
}
{BLOCK_MAC_ADDR} {
	yylval.text = yytext;
	return MACADDR;
}
{BLOCK_MAC_ADDR}"-"{BLOCK_MAC_ADDR} {
	yylval.text = yytext;
	return MACADDR_AREA;
}
{BLOCK_IP_ADDR} {
	yylval.text = yytext;	
	return IPADDR;	
}
{BLOCK_IP_ADDR}"-"{BLOCK_IP_ADDR} {
	yylval.text = yytext;		
	return IPADDR_AREA;	
}
"'"([0-9a-f]+)"'" {
	yylval.text = yytext;
	return HEX;
}
"("  return '(';
")"  return ')';
"["	 return '[';
"]"	 return ']';
"."  return '.';
"="  return '=';

[ \t\n]+ ;
.        yyerror("unknow");

%%

void yyerror(char* msg)
{
}

int yywrap(void) 
{
	return 1;
}