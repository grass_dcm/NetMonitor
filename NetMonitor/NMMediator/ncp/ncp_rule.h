#ifndef _NCP_RULE_H_
#define _NCP_RULE_H_

#pragma warning (disable:4616)
#pragma warning (disable:4786)
#include <map>
#include <vector>
#include "ncp_rule.h"
#include "ncp_parse.h"
extern "C"
{
	#include "grammar/grammar.h"
}

using namespace std;

/*===================================================================*/
/*rule struct                                                        */
/*===================================================================*/

/*rule struct*/
typedef struct 
{
	bool       enabled;
	string     md5;
	string     desc;
	def_node*  node;
	int        timeout;
	string     lock;
	string     unlock;
}def_rule;

typedef vector<def_rule*> def_rule_list;

/*stat struct*/
typedef struct
{
	def_node*		node;
	unsigned long	count;
}def_stat;

typedef map<string, def_stat*> def_stat_list;

/*===================================================================*/
/*function                                                           */
/*===================================================================*/

/*create rule vector*/
bool rule_create_vector();

/*free rule vector*/
bool rule_free_vector();

/* match function */
bool rule_match(def_packet_struct* packet_struct);

/* add and delete rule,stat */
bool rule_insert_rule(char* md5, char* xml);
bool rule_delete_rule(char* md5);
bool rule_insert_stat(char* md5, char* syntex);
bool rule_delete_stat(char* md5);

/* deal excute match */
def_bool grammar_deal_excute(def_operation_node* operation);

/* get stat count */
unsigned long rule_stat_count(char* md5);

#endif  _NCP_RULE_H_