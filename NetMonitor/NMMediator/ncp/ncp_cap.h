#ifndef _NCP_CAP_H_
#define _NCP_CAP_H_

extern "C"
{
#include "ncp_log.h"
}

typedef struct
{
	char*					device_name;
	int                     snaplen;
	int						flags/*PCAP_OPENFLAG_PROMISCUOUS*/;
	int						timeout;
	char*					filter_text;
	unsigned char			max_task_count;
	def_enum_log_io_type	log_io_type;
	char*					log_file_name;
}def_session_struct;

bool ncp_cap_start_cap(def_session_struct* session_struct);
bool ncp_cap_stop_cap();

#endif  _NCP_CAP_H_