#include <windows.h>
#include <process.h>    /* _beginthread, _endthread */
#include <algorithm>

#include "TaskScheduler.h"
#include "tinyxml/tinyxml.h"
#include "../include/NMMediator.h"
#include "util.h"
#include "logger.h"

bool                TaskScheduler::terminated = false;
CRITICAL_SECTION    TaskScheduler::_criticalSection;

TaskScheduler::TaskScheduler(void)
	: _threadHandle(NULL)
{
	InitializeCriticalSection(&_criticalSection);
}

TaskScheduler::~TaskScheduler(void)
{
	if ( !terminated ) terminateTimer();
	DeleteCriticalSection(&_criticalSection);
}

void TaskScheduler::initScheduler(TaskPropsMeta *pTaskProps)
{
	if (pTaskProps==NULL) return;

	pTaskProps->isPerInterval = ( pTaskProps->specified<=0 );
	if ( pTaskProps->isPerInterval )
	{
		//开始按时间间隔计时
		pTaskProps->triggerTickcout = GetTickCount() + pTaskProps->interval*1000;
	}
	else
	{
		//先计算到何时开始 "按时间间隔计时"
		pTaskProps->triggerTickcout = GetTickCount() + pTaskProps->specified*1000;
	}

	NM_TRACE("============================================================\n");
	NM_TRACE("initScheduler, isPerInterval=%d\n",    pTaskProps->isPerInterval);
	NM_TRACE("initScheduler, specified=%d\n",        pTaskProps->specified);
	NM_TRACE("initScheduler, interval=%d\n",         pTaskProps->interval);
	NM_TRACE("initScheduler, frequency=%d\n",        pTaskProps->frequency);
	NM_TRACE("initScheduler, triggerTickcout=%d\n",  pTaskProps->triggerTickcout);
	NM_TRACE("initScheduler, commandString=%s\n",    pTaskProps->commandString);
	NM_TRACE("============================================================\n");
}

bool TaskScheduler::hasItem(const char* md5Mark)
{
	if ((md5Mark == NULL) || (md5Mark[0]==0)) return true;

	bool ret = false;
	EnterCriticalSection(&_criticalSection);
	// Check if this item exists, if not, then insert it.
	if (_taskPropsMetaList.end() != _taskPropsMetaList.find(md5Mark)) {
		NM_TRACE("hasItem, md5Mark=%s can be found\n", md5Mark);
		ret =  true;
	}
	LeaveCriticalSection(&_criticalSection);
	return ret;
}

void TaskScheduler::addScheduler(const char* md5Mark, TaskPropsMeta *pTaskProps)
{
	if ((md5Mark == NULL) || (pTaskProps == NULL) || md5Mark[0]==0) {
		NM_ERROR("addScheduler, md5Mark=%x, pTaskProps=%x\n", md5Mark, pTaskProps);		
		return;
	}

	if ((pTaskProps->interval <= 0) || (pTaskProps->frequency<=0)) {
		NM_ERROR("addScheduler, illegal parameters(interval=%d, frequency=%d)\n", 
			pTaskProps->interval,
			pTaskProps->frequency);
		return;
	}

	initScheduler(pTaskProps);

	EnterCriticalSection(&_criticalSection);
	_taskPropsMetaList.insert( V_META_LIST::value_type(md5Mark, pTaskProps) );
	LeaveCriticalSection(&_criticalSection);	

	NM_TRACE("addScheduler, inserted md5Mark=%s successfully\n", md5Mark);
}

void TaskScheduler::deleteScheduler(const char* md5Mark)
{
	EnterCriticalSection(&_criticalSection);
	V_META_LIST::iterator item = _taskPropsMetaList.find(md5Mark);
	if ( _taskPropsMetaList.end()!=item ) {
		delete (*item).second;
		(*item).second = NULL;
		_taskPropsMetaList.erase( item );
	}
	LeaveCriticalSection(&_criticalSection);
}

void TaskScheduler::addScheduler(const char* md5Mark, const char *xmlProps)
{
	if ( hasItem(md5Mark) ) return;

	TiXmlDocument xml;
	TaskPropsMeta *pTaskProps = new TaskPropsMeta();
	memset( pTaskProps, 0, sizeof(TaskPropsMeta) );

	xml.Parse(xmlProps);
	TiXmlElement* rootElem = xml.FirstChildElement( "taskprops" );
	if ( rootElem )
	{
		const char *nodeValue;
		nodeValue = rootElem->FirstChildElement("specified")->GetText();
		pTaskProps->specified = strtoul(CHK_NULL_S(nodeValue), NULL, 10);

		nodeValue = rootElem->FirstChildElement("interval")->GetText();
		pTaskProps->interval = strtoul(CHK_NULL_S(nodeValue), NULL, 10);
		if (pTaskProps->interval<=0) return;

		nodeValue = rootElem->FirstChildElement("frequency")->GetText();
		pTaskProps->frequency = strtoul(CHK_NULL_S(nodeValue), NULL, 10);
		if (pTaskProps->frequency<=0) return;

		pTaskProps->desc.append( CHK_NULL_S(rootElem->FirstChildElement("desc")->GetText()) );
		pTaskProps->commandString.append( CHK_NULL_S(rootElem->FirstChildElement("command")->GetText()) );

		// add into map, structor is TaskPropsMeta
		addScheduler( md5Mark, pTaskProps );
	}
}
/*
bool TaskScheduler::propsGreater(TaskPropsMeta *pAObj, TaskPropsMeta *pBObj)
{
	return (pAObj->specified) < (pBObj->specified);
}
*/
void TaskScheduler::enableTimer()
{
	if ( _threadHandle!=NULL ) return;

	terminated = false;
	unsigned int	threadid;
/*
	V_META_LIST::iterator begin = _taskPropsMetaList.begin();
	V_META_LIST::iterator end = _taskPropsMetaList.end();

	// should sort first per specified.
	sort(begin, end, propsGreater);

	for (; begin!=end; begin++) 
	{
		initScheduler( (*begin).second );
	}
*/
	_threadHandle = (HANDLE)_beginthreadex(0, 0, timerEnventCallback, &_taskPropsMetaList, 0, &threadid);
}

unsigned int TaskScheduler::timerEnventCallback(void *pObject)
{
	V_META_LIST *pTaskPropsMetaList = (V_META_LIST *)pObject;
	V_META_LIST::iterator begin;
	V_META_LIST::iterator end;

	int startTime = GetTickCount();
	NM_DEBUG("timerEnventCallback, start time... %d\n", startTime);
	while ( !terminated )
	{
		EnterCriticalSection(&_criticalSection);
		begin = pTaskPropsMetaList->begin();
		end = pTaskPropsMetaList->end();

		while ( begin!=end )
		{
			int tickount = GetTickCount();
			//如果重复触发到最大次数
			if ( (*begin).second->frequency==0 ) 
			{
				delete (*begin).second;
				(*begin).second = NULL;
				pTaskPropsMetaList->erase( begin );

				begin = pTaskPropsMetaList->begin();
				end = pTaskPropsMetaList->end();
				NM_TRACE("timerEnventCallback, %s was deleted\n", (*begin).first.c_str());
				continue;
			}

			if ( tickount>=(*begin).second->triggerTickcout )
			{
				if ( (*begin).second->isPerInterval )
				{
					NM_TRACE("timerEnventCallback, interval used time (ms)=%d\n", tickount-startTime);
					(*begin).second->triggerTickcout = tickount + (*begin).second->interval*1000;
					if ( (*begin).second->frequency>0 ) {
						(*begin).second->frequency--;
					}

					NM_sendCommandToDevice(auto_command_protocol, (*begin).second->commandString.c_str());
					//事件回调机制
					EventObject paramObj;
					paramObj.comefrom = 2;
					paramObj.md5Mark = (*begin).first.c_str();
					paramObj.desc = (*begin).second->desc.c_str();
					memset(&paramObj.packet, 0, sizeof(RuleIPPacket));
					if ( fireLoggerEventCallbak!=NULL )
						fireLoggerEventCallbak( paramObj );
				}
				else
				{
					NM_TRACE("timerEnventCallback, specified used time (ms)=%d\n", tickount-startTime);
					(*begin).second->triggerTickcout = tickount + (*begin).second->specified*1000;
					(*begin).second->isPerInterval = true;
				}
			}

			begin++;
		}

		LeaveCriticalSection(&_criticalSection);
		_sleep(500);
	}
	return 0;
}

void TaskScheduler::terminateTimer()
{
	if ( _threadHandle==NULL ) return;

	terminated = true;
	WaitForSingleObject(_threadHandle, INFINITE);
	CloseHandle(_threadHandle);
	_threadHandle = NULL;

	V_META_LIST::iterator begin = _taskPropsMetaList.begin();
	V_META_LIST::iterator end = _taskPropsMetaList.end();
	for (; begin!=end; begin++)
	{
		delete (*begin).second;
		(*begin).second = NULL;
	}

	_taskPropsMetaList.clear();
	NM_DEBUG("TaskScheduler thread exits successfully\n");
}