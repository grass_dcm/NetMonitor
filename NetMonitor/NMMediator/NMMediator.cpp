/**
 * NMMediator Library
 * 
 * Copyright (C) 2007-208 keesion <keesion@gmail.com>
 */

#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <string.h>

#include "ncp/ncp_cap.h"
#include "ncp/ncp_rule.h"
#include "pcap.h"

#include "tinyxml/tinyxml.h"

#include "sqlite3/sqlite3.h"
#include "md5/md5class.h"
#include "../include/NMMediator.h"
#include "TaskScheduler.h"
#include "telnet/telnet.h"

#include "util.h"
#include "logger.h"
#include "des/des.h"

// snmp dll include header
#include "../SNMPDll/SNMPDll.h"
#include "Expire.h"

/**
 * Global variables
 */
/** sqlite db handle */
sqlite3 *sqlite_db_handle = NULL;

/** logger evnent callback function pointer */
EventCallback fireLoggerEventCallbak = NULL;

#define ERROR_BUFFER_SIZE 256
char last_error_message[ERROR_BUFFER_SIZE]={0};

/** buffer used for saving md5 string  */
char md5_buffer_32_chars[33] = {0};

TaskScheduler taskSchedulerObject;
bool core_system_is_running = false;

// Get from local database
char *remote_device_ip = "";
TELNET_PROPS    telnetPropsObject;

char *ssh_username = NULL;
char *ssh_password = NULL;
int   ssh_port = 22;
char *ssh_key = NULL;

char auto_command_protocol = 0x02;  //default is Telnet

void setErrorMessage(const char *format, ...)
{
	va_list ap;
	char *buffer;
	va_start(ap, format);

	buffer = sqlite3_vmprintf(format, ap);
	NM_DEBUG("[setErrorMessage]: %s\n", buffer);

	int len = strlen(buffer);
	if ( len>ERROR_BUFFER_SIZE) len = ERROR_BUFFER_SIZE-1;

	strncpy(last_error_message, buffer, len);
	sqlite3_free(buffer);
	buffer = NULL;

	va_end(ap);
}

void SET_SQLITE_ERROR()
{
	if ( sqlite_db_handle!=NULL ) {
		const char *errmsg = sqlite3_errmsg(sqlite_db_handle);
		if (strcmp(errmsg, "not an error")==0) return;
		setErrorMessage( errmsg );
	}
}

int sqlite3_exec_printf(const char *sqlFormat, ...)
{
	va_list ap;
	char *sql = NULL;
	int result;

	va_start(ap, sqlFormat);

	sql = sqlite3_vmprintf(sqlFormat, ap);
	result = sqlite3_exec(sqlite_db_handle, sql, NULL, 0, NULL);
	SET_SQLITE_ERROR();
	sqlite3_free(sql);
	sql = NULL;
	
	va_end(ap);
	return result;
}

char *calculate_md5_chars(const char *format, ...)
{
	va_list ap;
	CMD5 md5Object;
	char *buffer;
	va_start(ap, format);

	buffer = sqlite3_vmprintf(format, ap);
	md5Object.setPlainText(buffer);
	strcpy(md5_buffer_32_chars, md5Object.getMD5Digest());
	
	sqlite3_free(buffer);
	buffer = NULL;

	va_end(ap);
	return md5_buffer_32_chars;
}

void updateTelnetElems(const char *xmlProps)
{
	telnetPropsObject.metaData.clear();
	if ( !xmlProps ) return;
	TiXmlDocument xml;
// Convert a string to lowercase
// strlwr(xmlProps);
	xml.Parse(xmlProps);
	TiXmlElement* rootElem = xml.FirstChildElement( "telnet" );
	TiXmlElement* subElem;
	if ( rootElem )
	{
		const char *nodeValue;
		nodeValue = rootElem->Attribute("port");
		if ( nodeValue ) {
			telnetPropsObject.port = strtoul(nodeValue, NULL, 10);
		}
		nodeValue = rootElem->Attribute("prompt");
		if ( nodeValue ) {
			strncpy(telnetPropsObject.prompt, nodeValue, 31);
		}

		subElem = rootElem->FirstChildElement("negotiation");
		while ( subElem ) {
			TELNET_META meta;
			if ( (subElem->Attribute("keyword")) && (subElem->Attribute("value")) ) {
				meta.keyword.append(subElem->Attribute("keyword"));
				meta.value.append(subElem->Attribute("value"));
				telnetPropsObject.metaData.push_back(meta);
			}
			subElem = subElem->NextSiblingElement();
		}
	}
}

void initGlobalValues()
{
//	memset(&telnetPropsObject, 0, sizeof(TELNET_PROPS));
	telnetPropsObject.port = 23;
	remote_device_ip = NM_getRemoteIp();
	NM_getSSHConfig(&ssh_username, &ssh_password, &ssh_port, &ssh_key);
	char *xmlProps = NM_getTelnetConfig();
	updateTelnetElems(xmlProps);
	NM_free(xmlProps);

	CHECK_NULL(remote_device_ip);
	CHECK_NULL(ssh_username);
	CHECK_NULL(ssh_password);
	CHECK_NULL(ssh_key);
}

void freeGlobalValues()
{
	FREE_AND_NULL(remote_device_ip);
	FREE_AND_NULL(ssh_username);
	FREE_AND_NULL(ssh_password);
	FREE_AND_NULL(ssh_key);
}

bool STDCALL NM_hasRecordset(const char *sql)
{
	char **table;
	int rows, cols, result;

	if ( sqlite_db_handle==NULL )
	{
	    setErrorMessage("sqlite db handle is NULL, maybe you forgot opening database");
	    return false;
	}
	result = sqlite3_get_table(sqlite_db_handle, sql, &table, &rows, &cols, NULL);
	SET_SQLITE_ERROR();
	sqlite3_free_table(table);

	if ( (rows==0) || (result!=SQLITE_OK) ) return false;
	return true;
}

int STDCALL NM_executeSQL(const char *sql)
{
	if ( !sqlite_db_handle ) return NMS_FAILED;

	int result = sqlite3_exec_printf(sql);
	SET_SQLITE_ERROR();
	return (result==SQLITE_OK) ? NMS_SUCCESS:NMS_FAILED;
}

char * sqlite_insert_record(const char *tableName, 
							const char *propsFieldValue)
{
	int result;
	char *md5Chars;
	char buffer[256];

	md5Chars = calculate_md5_chars("%s", propsFieldValue);
	sprintf(buffer, "select %s from %s where %s='%s'\0", 
		MD5_MARK_FIELD_NAME, tableName, MD5_MARK_FIELD_NAME, md5Chars);
	if ( NM_hasRecordset(buffer) ) return NULL;

	result = sqlite3_exec_printf(
	    "insert into %s (%s, %s) values('%q', '%s')", 
		tableName, PROPS_FIELD_NAME, MD5_MARK_FIELD_NAME,
	    propsFieldValue, md5Chars);

	if ( result!=SQLITE_OK ) {
		SET_SQLITE_ERROR();
	    md5Chars = NULL;
	}

	return md5Chars;
}

int sqlite_update_record(const char *tableName, 
						 const char *idFieldValue,
						 const char *propsFieldValue)
{
	int result;

	result = sqlite3_exec_printf(
	    "update %s set %s='%q' where %s='%s'",
	    tableName, PROPS_FIELD_NAME, propsFieldValue,
		MD5_MARK_FIELD_NAME, idFieldValue);

	SET_SQLITE_ERROR();
	return (result==SQLITE_OK) ? NMS_SUCCESS:NMS_FAILED;
}

int sqlite_delete_record(const char *tableName, 
						 const char *idFieldValue)
{
	int result;

	result = sqlite3_exec_printf(
	    "delete from %s where %s='%s'", tableName, MD5_MARK_FIELD_NAME, idFieldValue);

	SET_SQLITE_ERROR();
	return (result==SQLITE_OK) ? NMS_SUCCESS:NMS_FAILED;
}

char **sqlite_get_record_list(const char *sql, int *count)
{
	char **table;
	char **result_list = NULL;
	int rows, cols, result, k, index;

	result = sqlite3_get_table(sqlite_db_handle, sql, &table, &rows, &cols, NULL);
	*count = 0;

	for (k = 0; (k<cols) && (rows>0); k++)
	{
	    if (strcmp(table[k], MD5_MARK_FIELD_NAME) == 0) {
	        result_list = (char **)NM_malloc(rows*sizeof(char*));
	        *count = rows;

	        for (index = 0; index < rows; index++) {
	            char *fieldValue = table[k+cols*(index+1)];
	            result_list[index] = strdup(fieldValue);
	        }

	        break;
	    }
	}

	sqlite3_free_table(table);
	SET_SQLITE_ERROR();
	return result_list;
}

char *sqlite_get_record_props_text(const char *tableName,
								   const char *idFieldValue)
{
	char **table;
	char *fieldValue;
	char *xmlProps = NULL;
	int rows, cols, result, k;

	char *sql = sqlite3_mprintf("select %s from %s where %s='%s'",
		PROPS_FIELD_NAME, tableName, 
		MD5_MARK_FIELD_NAME, idFieldValue);

	result = sqlite3_get_table(sqlite_db_handle, sql, &table, &rows, &cols, NULL);

	for (k = 0; (k<cols) && (rows>0); k++)
	{
	    if (strcmp(table[k], PROPS_FIELD_NAME) == 0) {
	        fieldValue = table[k+cols];
	        xmlProps = strdup(fieldValue);
	        break;
	    }
	}

	sqlite3_free_table(table);
	sqlite3_free(sql);
	SET_SQLITE_ERROR();
	return xmlProps;
}

/**
 * Rule Group
 */
char * STDCALL NM_insertRule(const char *xmlProps)
{
	char *md5Chars = sqlite_insert_record("RuleTable", xmlProps);
	if ( core_system_is_running && md5Chars ) {
		rule_insert_rule(md5Chars, (char *)xmlProps);
	}
	return md5Chars;
}

int STDCALL NM_updateRule(const char *ruleMark, const char *xmlProps)
{
	return sqlite_update_record("RuleTable", ruleMark, xmlProps);
}

int STDCALL NM_removeRule(const char *ruleMark)
{
	int ret = sqlite_delete_record("RuleTable", ruleMark);
	if ( core_system_is_running ) {
		ret = rule_delete_rule((char *)ruleMark) ? NMS_SUCCESS : NMS_FAILED;
	}
	return ret;
}

char** STDCALL NM_getRuleList(const char *sql, int *count)
{
	return sqlite_get_record_list(sql, count);
}

char * STDCALL NM_getRuleProps(const char *ruleMark)
{
	return sqlite_get_record_props_text("RuleTable", ruleMark);
}

/** 
 * Task Group
 */
char * STDCALL NM_insertTask(const char *xmlProps)
{
	char *md5Chars = sqlite_insert_record("TaskTable", xmlProps);
	if ( core_system_is_running && md5Chars) {
		taskSchedulerObject.addScheduler(md5Chars, xmlProps);
	}
	return md5Chars;
}

int STDCALL NM_updateTask(const char *taskMark, const char *xmlProps)
{
	return sqlite_update_record("TaskTable", taskMark, xmlProps);
}

int STDCALL NM_removeTask(const char *taskMark)
{
	int ret = sqlite_delete_record("TaskTable", taskMark);
	if ( core_system_is_running ) {
		taskSchedulerObject.deleteScheduler(taskMark);
	}
	return ret;
}

char** STDCALL NM_getTaskList(const char *sql, int *count)
{
	return sqlite_get_record_list(sql, count);
}

char * STDCALL NM_getTaskProps(const char *taskMark)
{
	return sqlite_get_record_props_text("TaskTable", taskMark);
}

/**
 * Statistic Group
 */
char * STDCALL NM_insertStats(const char *syntax)
{
	char *md5Chars = sqlite_insert_record("StatsTable", syntax);
	if ( core_system_is_running && md5Chars ) {
		rule_insert_stat(md5Chars, (char *)syntax);
	}
	return md5Chars;
}

int STDCALL NM_updateStats(const char *statsMark, const char *syntax)
{
	return sqlite_update_record("StatsTable", statsMark, syntax);
}

int STDCALL NM_removeStats(const char *statsMark)
{
	int ret = sqlite_delete_record("StatsTable", statsMark);
	if ( core_system_is_running ) {
		ret = rule_delete_stat((char *)statsMark) ? NMS_SUCCESS : NMS_FAILED; 
	}
	return ret;
}

char** STDCALL NM_getStatsList(const char *sql, int *count)
{
	return sqlite_get_record_list(sql, count);
}

char * STDCALL NM_getStatsProps(const char *statsMark)
{
	return sqlite_get_record_props_text("StatsTable", statsMark);
}

/**
 * Miscellaneous API encapsulation
 */

int STDCALL NM_setAutoCommandProtocol(int protocol)
{
	if ((protocol == 0x01) || (protocol == 0x02)) {
		auto_command_protocol = protocol;
		return NMS_SUCCESS;
	}
	
	return NMS_FAILED;
}

int STDCALL NM_sendCommandToDevice(int protocol, const char *commandString)
{
	if ((commandString == NULL) || (commandString[0]==0)) {
		NM_WARN("NM_sendCommandToDevice, commandString is NULL or empty\n");
		return NMS_FAILED;
	}

	bool ret = false;
	if ( protocol==1 ) {
		PROCESS_INFORMATION pi;
		STARTUPINFO si;
		GetStartupInfo(&si);
		si.dwFlags = (STARTF_USESHOWWINDOW | STARTF_USESTDHANDLES);
		si.wShowWindow = SW_HIDE;
		char *exefile = "plink.exe";
		char *argv = sqlite3_mprintf("%s -P %d -l %s -pw \"%s\" -batch %s \"%s\"", 
			exefile, ssh_port, ssh_username, ssh_password, remote_device_ip, commandString);
		NM_DEBUG("NM_sendCommandToDevice, argv=(%s)\n", argv);
		ret = CreateProcess(exefile, argv, NULL, NULL,
			TRUE, CREATE_NEW_CONSOLE, NULL, NULL, &si, &pi);
		sqlite3_free(argv);

//		ret = bReturn==0 ? false:true;
	}
	else if ( protocol==2 && telnetPropsObject.status==eMessage ) {
		ret = telnet_send( commandString, strlen(commandString) );
		NM_DEBUG("NM_sendCommandToDevice, telnet command=%s, result=%d\n", commandString, ret);
	}

	return ret ? NMS_SUCCESS : NMS_FAILED;
}

long STDCALL NM_getLocalIOBytes(const char *statsMark)
{
	if ( !statsMark ) return 0;
	return rule_stat_count( (char *)statsMark );
}

long STDCALL NM_getLocalIOPackets(const char *statsMark)
{
	if ( !statsMark ) return 0;
	return 0;
}

void STDCALL NM_setEventCallback(EventCallback fireEventObj)
{
	if ( fireEventObj!=NULL )
		fireLoggerEventCallbak = fireEventObj;
}

int STDCALL NM_startMonitor(const char* device, int snaplen, 
	                        bool promiscuous, int timeout, 
	                        const char *filter)
{
#ifdef NEED_CHECK_DATE
	CExpire Protector("{3121E414-15C5-11DD-B640-5B9B56D89593}",	"InprocServer32", ALLOW_RUN_DAYS, TYPEDAYS);
	if ( Protector.HasExpired() )
	{
		NM_ERROR("Kernel prolbems, you should contact the developer\n");
		exit(-1);
		return NMS_FAILED;
	}
#endif
	NM_DEBUG("device=%s, snaplen=%d, promiscuous=%d, timeout=%d, filter=%s\n",
		device, snaplen, promiscuous, timeout, filter?filter:"");

	//connect telnet server
	if (remote_device_ip==NULL) {
		NM_ERROR("Remote device IP is NULL, system will be terminated\n");
		return NMS_FAILED;
	}

	NM_DEBUG("Will telnet to %s, the port is:%d\n", remote_device_ip, telnetPropsObject.port);
	telnet(remote_device_ip, telnetPropsObject.port);

	int count, index;
	char **list = NM_getTaskList(SELECT_FROM_TABLE(TaskTable), &count);

	for (index=0; index<count; index++)
	{
	    char *xmlProps = NM_getTaskProps(list[index]);
	    taskSchedulerObject.addScheduler(list[index], xmlProps);
	    NM_free(xmlProps);
	}
	NM_freeList(list, count);
	taskSchedulerObject.enableTimer();
	NM_DEBUG("Task scheduler has been enabled\n");

	def_session_struct session_struct;
	session_struct.device_name    = (char *)device;
	session_struct.flags          = promiscuous;
	session_struct.timeout        = timeout;
	session_struct.filter_text    = (char *)filter;
	session_struct.max_task_count = 5;
	session_struct.log_io_type    = enum_log_io_type_std;
	session_struct.log_file_name  = NULL;
	
	if ( !ncp_cap_start_cap(&session_struct) ) return NMS_FAILED;
	core_system_is_running = true;
	return NMS_SUCCESS;
}

int STDCALL NM_stopMonitor()
{
	close_telnet();
	NM_DEBUG("Disconnected from telnet server\n");
	taskSchedulerObject.terminateTimer();
	NM_DEBUG("Task scheduler thread has been terminated\n");
	ncp_cap_stop_cap();
	NM_DEBUG("Network packet capture thread has been terminated\n");
	core_system_is_running = false;
	return NMS_SUCCESS;
}

void* STDCALL NM_malloc(int size)
{
	return malloc(size);
}

void STDCALL NM_free(void *pObject)
{
	if ( pObject!=NULL )
	    free(pObject);
}

void STDCALL NM_freeList(char **ppObject, int count)
{
	int index;

	for (index = 0; index < count; index++) {
	    NM_free(ppObject[index]);
	    ppObject[index] = NULL;
	}

	NM_free(ppObject);
}

int STDCALL NM_openDatabase(char *fileName)
{
	NM_closeDatabase();

	if ( fileName==NULL ) {
	    fileName = DEFAULT_DB_NAME;
	}
	else if ( strcmp(fileName, "")==0 ) {
	    fileName = DEFAULT_DB_NAME;
	}

	int result;
	result = sqlite3_open(fileName, &sqlite_db_handle);
	if ( result!=SQLITE_OK ) {
		SET_SQLITE_ERROR();
		sqlite_db_handle = NULL;
	    return NMS_FAILED;
	}

	NM_DEBUG("Local database has been opened, sqlite_db_handle=%x\n", sqlite_db_handle);
	NM_DEBUG("SQLite3 thread safe status is %d\n", sqlite3_threadsafe());
	initGlobalValues();
	return NMS_SUCCESS;
}

int STDCALL NM_closeDatabase()
{
/*
	if ( sqlite_db_handle==NULL ) 
	{
		setErrorMessage("sqlite_db_handle is NULL");
		return -2;
	}
*/
	if ( sqlite_db_handle!=NULL ) {
	    sqlite3_close(sqlite_db_handle);
	    sqlite_db_handle = NULL;
		freeGlobalValues();
		NM_DEBUG("Local database has been closed\n");
	}

	return NMS_SUCCESS;
}

const char* STDCALL NM_getLastError()
{
	return last_error_message;
}

char ** STDCALL NM_lookupDevices(int *count)
{
	pcap_if_t *alldevs;
	pcap_if_t *d;
	char errbuf[PCAP_ERRBUF_SIZE];

	if ( pcap_findalldevs(&alldevs, errbuf)==-1 )
	{
		setErrorMessage("[NM_lookupDevices]: Error in pcap_findalldevs: %s\n", errbuf);
		return NULL;
	}

	int items = 0;
	//计算网卡数量
	for(d=alldevs; d!=NULL; d=d->next)
	{
		items++;
	}

	*count = items;
	char **dev_name_list = NULL;
	dev_name_list = (char **)NM_malloc( sizeof(char *)*items );

	//向列表中赋值
	for(d=alldevs, items=0; d!=NULL; d=d->next, items++)
	{
		dev_name_list[items] = strdup(d->name);
	//	int size = strlen(d->name);
	//	dev_name_list[items] = (char *)NM_malloc( size+1 );
	//	strcpy(dev_name_list[items], d->name);
	}

	pcap_freealldevs(alldevs);
	return dev_name_list;
}


/**
 * Local configuration, including ip, telnet and snmp
 */
int STDCALL NM_setRemoteIp(char *ip)
{
	if ( !ip ) return NMS_FAILED;

	int result = sqlite3_exec_printf("update ConfigTable set remote_ip='%s'", ip);
	if ( result!=SQLITE_OK ) {
		SET_SQLITE_ERROR();
		return NMS_FAILED;
	}

//	if ( core_system_is_running ) {
		FREE_AND_NULL(remote_device_ip);
		remote_device_ip = strdup(ip);
//	}
	return NMS_SUCCESS;
}

char * STDCALL NM_getRemoteIp()
{
	char *ip = NULL;
	char **table;
	int rows, cols, result, k;

	char *sql = sqlite3_mprintf("select remote_ip from ConfigTable");
	result = sqlite3_get_table(sqlite_db_handle, sql, &table, &rows, &cols, NULL);

	for (k = 0; (k<cols) && (rows>0); k++)
	{
	    if (strcmp(table[k], "remote_ip") == 0) {
	        ip = strdup( table[k+cols] );
	    }
	}

	sqlite3_free(sql);
	sqlite3_free_table(table);
	SET_SQLITE_ERROR();

	return ip;
}

int STDCALL NM_setTelnetConfig(const char *xmlProps)
{
	if ( !xmlProps) return NMS_FAILED;
	int result = sqlite3_exec_printf("update ConfigTable set TelnetXMLProps='%s'", xmlProps);

	if ( result!=SQLITE_OK ) {
		SET_SQLITE_ERROR();
		return NMS_FAILED;
	}

	updateTelnetElems(xmlProps);
	return NMS_SUCCESS;
}

char * STDCALL NM_getTelnetConfig()
{
	char **table;
	char *fieldValue, *xmlProps;
	int rows, cols, result, k;

	char *sql = "select TelnetXMLProps from ConfigTable";
	result = sqlite3_get_table(sqlite_db_handle, sql, &table, &rows, &cols, NULL);

	for (k = 0; (k<cols) && (rows>0); k++)
	{
	    if (strcmp(table[k], "TelnetXMLProps") == 0) {
	        fieldValue = table[k+cols];
	        xmlProps = strdup(fieldValue);
	    }
	}

	sqlite3_free_table(table);
	SET_SQLITE_ERROR();

	return xmlProps;
}

int STDCALL NM_setSnmpConfig(int version, char *mibspath, char *security, char *password)
{
	char *des_password = des_encrypt(password);
	if ( !mibspath || !security || !des_password ) return NMS_FAILED;

	int result = sqlite3_exec_printf("update ConfigTable set snmp_version=%d, snmp_mibspath='%s', snmp_security='%s', snmp_password='%s'", version, mibspath, security, des_password);
	FREE_AND_NULL(des_password);

	if ( result!=SQLITE_OK ) {
		SET_SQLITE_ERROR();
		return NMS_FAILED;
	}

	return NMS_SUCCESS;
}

int STDCALL NM_getSnmpConfig(int *version, char **mibspath, char **security, char **password)
{
	char **table;
	char *fieldValue;
	int rows, cols, result, k;

	char *sql = "select snmp_version, snmp_mibspath, snmp_security, snmp_password from ConfigTable";
	result = sqlite3_get_table(sqlite_db_handle, sql, &table, &rows, &cols, NULL);

	for (k = 0; (k<cols) && (rows>0); k++)
	{
	    if (strcmp(table[k], "snmp_version") == 0) {
	        fieldValue = table[k+cols];
			*version = STR2INT(fieldValue);
	    }

		if (strcmp(table[k], "snmp_mibspath") == 0) {
	        fieldValue = table[k+cols];
	        *mibspath = strdup(fieldValue);
	    }

		if (strcmp(table[k], "snmp_security") == 0) {
	        fieldValue = table[k+cols];
	        *security = strdup(fieldValue);
	    }

		if (strcmp(table[k], "snmp_password") == 0) {
			char *cipher = NULL;
	        fieldValue = table[k+cols];
			cipher = des_decrypt(fieldValue);
	        *password = strdup(cipher);
			NM_free(cipher);
	    }
	}

	sqlite3_free_table(table);
	SET_SQLITE_ERROR();

	if ( *mibspath==NULL || *security==NULL || *password==NULL ) return NMS_FAILED;
	return NMS_SUCCESS;
}

int STDCALL NM_openSnmpServer()
{
	int version = 1;
	char *mibspath = NULL;
	char *security = NULL;
	char *password = NULL;
	char *ip = NM_getRemoteIp();
	bool isOpen;

	int ret = NM_getSnmpConfig(&version, &mibspath, &security, &password);
	if ( ret==NMS_FAILED ) goto __END;

	isOpen = dll_snmp_open(mibspath, ip, version==3, security, password);
	ret = isOpen ? NMS_SUCCESS : NMS_FAILED;

__END:
	FREE_AND_NULL(ip);
	FREE_AND_NULL(mibspath);
	FREE_AND_NULL(security);
	FREE_AND_NULL(password);
	return ret;
}

int STDCALL NM_sendSnmpObjectMeta(char *oids, int type, char *value, int len)
{
	if ( !oids || !value ) return NMS_FAILED;
	bool ret = dll_snmp_command(oids, type, (unsigned char *)value, len);
	return ret ? NMS_SUCCESS : NMS_FAILED;
}

int STDCALL NM_getSnmpObjectMeta(char *oids, int *type, char **ppValue, int *len)
{
	char data[256] = {0};
	unsigned char typeOfByte;
	int retLen = dll_snmp_get_info(oids, &typeOfByte, data, 255);
	*type = typeOfByte;
	*len = retLen;

	if ( retLen>0 ) {
		(*ppValue) = (char *)NM_malloc(retLen);
		memcpy(*ppValue, data, retLen);
	}
	else
		(*ppValue) = NULL;

	return retLen!=-1 ? NMS_SUCCESS : NMS_FAILED;
}

int STDCALL NM_closeSnmpServer()
{
	bool bClosed = dll_snmp_close();
	return bClosed ? NMS_SUCCESS : NMS_FAILED;
}

int STDCALL NM_setSSHConfig(char *username, char *password, int port, char *key)
{
	char *des_password = des_encrypt(password);
	if ( !des_password || !username ) return NMS_FAILED;

	int result = sqlite3_exec_printf("update ConfigTable set ssh_username='%s', ssh_password='%s', ssh_port=%d, ssh_key='%s'", username, des_password, port, key);
	FREE_AND_NULL(des_password);

	if ( result!=SQLITE_OK ) {
		SET_SQLITE_ERROR();
		return NMS_FAILED;
	}

//	if ( core_system_is_running ) {
		FREE_AND_NULL(ssh_username);
		FREE_AND_NULL(ssh_password);
		FREE_AND_NULL(ssh_key);

		ssh_username = strdup(username);
		ssh_password = strdup(password);
		ssh_key = strdup(key);
//	}
	return NMS_SUCCESS;
}

int STDCALL NM_getSSHConfig(char **username, char **password, int *port, char **key)
{
	char **table;
	char *fieldValue;
	int rows, cols, result, k;

	char *sql = "select ssh_username, ssh_password, ssh_port, ssh_key from ConfigTable";
	result = sqlite3_get_table(sqlite_db_handle, sql, &table, &rows, &cols, NULL);

	for (k = 0; (k<cols) && (rows>0); k++)
	{
	    if (strcmp(table[k], "ssh_username") == 0) {
			fieldValue = table[k+cols];
			*username = strdup(fieldValue);
	    }

		if (strcmp(table[k], "ssh_password") == 0) {
			char *cipher = NULL;
	        fieldValue = table[k+cols];
			cipher = des_decrypt(fieldValue);
	        *password = strdup(cipher);
			FREE_AND_NULL(cipher);
	    }

	    if (strcmp(table[k], "ssh_port") == 0) {
	        fieldValue = table[k+cols];
			*port = STR2INT(fieldValue);
	    }

	    if (strcmp(table[k], "ssh_key") == 0) {
	        fieldValue = table[k+cols];
	        *key = strdup(fieldValue);
	    }
	}

	sqlite3_free_table(table);
	SET_SQLITE_ERROR();

	return NMS_SUCCESS;
}

int STDCALL NM_setMapEnabled(bool bEnabled)
{
	map_set_enable(bEnabled);
	return NMS_SUCCESS;
}

char ** STDCALL NM_getIpsMapTable(int *count)
{
	return map_get_table(count);
}

char * STDCALL NM_getMacByIp(char *ip)
{
	return map_get_mac_by_ip(ip);
}

int STDCALL NM_clearIpsMapTable()
{
	map_clear_table();
	return NMS_SUCCESS;
}