#ifndef __DES_H
#define __DES_H

#ifdef __cplusplus
extern "C" {
#endif

char *des_encrypt(const char *from);
char *des_decrypt(const char *from);

#ifdef __cplusplus
}
#endif

#endif