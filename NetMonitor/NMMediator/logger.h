#ifndef __LOGGER_H
#define __LOGGER_H

/**
 * logger system
 */
#define USE_LOGGER
#ifdef  USE_LOGGER

extern void logger_trace(const char *format, ...);
extern void logger_debug(const char *format, ...);
extern void logger_info(const char *format, ...);
extern void logger_warn(const char *format, ...);
extern void logger_error(const char *format, ...);
extern void logger_fatal(const char *format, ...);

#define NM_TRACE logger_trace
#define NM_DEBUG logger_debug
#define NM_INFO  logger_info
#define NM_WARN  logger_warn
#define NM_ERROR logger_error
#define NM_FATAL logger_fatal

#else //else #ifdef USE_LOGGER

#define NM_TRACE
#define NM_DEBUG
#define NM_INFO
#define NM_WARN
#define NM_ERROR
#define NM_FATAL

#endif  //end #ifdef USE_LOGGER

#endif