#ifndef __TASK_SCHEDULER_H
#define __TASK_SCHEDULER_H

#pragma warning(disable:4786)
#include <windows.h>
#include <string>
#include <iostream>
#include <map>

using namespace std;

#ifdef __cplusplus
extern "C" {
#endif

/**
 * 本类内部用
 * @struct TaskPropsMeta
 */
typedef struct _TASK_PROPS_META_ {
	/**
	 * 是否已经开始以interval触发<br>
	 *      isPerInterval=true 以interval触发
	 *      isPerInterval=false 代表要经过[specified]秒后触发
	 */
	bool        isPerInterval;
	/**
	 * 即将触发的时间，由GetTickCount返回，代表自PC运行起经过多少毫秒(ms)
	 */
	int         triggerTickcout;

	/**
	 * 任务开始后，经过多久开始以interval间隔触发
	 */
	long        specified;
	/**
	 * Timer触发的时间间隔，以秒为单位。也就是每隔多久向设备发送commandString所指定的命令
	 */
	int         interval;
	/**
	 * 要触发的次数
	 */
	int         frequency;
	/**
	 * 任务的描述信息
	 */
	string      desc;
	/**
	 * 向设备发送的命令。<br>
	 * 此命令为路由器或交换机等设备支持的命令，命令中已加入相应的IP,端口,MAC等参数
	 */
	string      commandString;

} TaskPropsMeta;

typedef map<string, TaskPropsMeta*> V_META_LIST;
//typedef vector<TaskPropsMeta *> V_META_LIST;

class TaskScheduler
{
private:
	static CRITICAL_SECTION       _criticalSection;
	HANDLE                        _threadHandle;
	static bool                   terminated;
	V_META_LIST                   _taskPropsMetaList;
//	static bool                   propsGreater(TaskPropsMeta *pAObj, TaskPropsMeta *pBObj);
	static unsigned int __stdcall timerEnventCallback(void *pObject);

public:
	void                          initScheduler(TaskPropsMeta *pTaskProps);
	bool                          hasItem(const char* md5Mark);
	void                          deleteScheduler(const char* md5Mark);
	void                          addScheduler(const char* md5Mark, TaskPropsMeta *pTaskProps);
	void                          addScheduler(const char* md5Mark, const char *xmlProps);
	void                          enableTimer();
	void                          terminateTimer();
	TaskScheduler(void);

public:
	~TaskScheduler(void);
};

#ifdef __cplusplus
}
#endif
#endif