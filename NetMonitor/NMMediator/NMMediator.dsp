# Microsoft Developer Studio Project File - Name="NMMediator" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Dynamic-Link Library" 0x0102

CFG=NMMediator - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "NMMediator.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "NMMediator.mak" CFG="NMMediator - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "NMMediator - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "NMMediator - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "NMMediator - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "NMMediator___Win32_Release"
# PROP BASE Intermediate_Dir "NMMediator___Win32_Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "NMMediator___Win32_Release"
# PROP Intermediate_Dir "NMMediator___Win32_Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MT /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "NMMEDIATOR_EXPORTS" /YX /FD /c
# ADD CPP /nologo /MT /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "NMMEDIATOR_EXPORTS" /YX /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x804 /d "NDEBUG"
# ADD RSC /l 0x804 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /dll /machine:I386
# ADD LINK32 sqlite3\sqlite3.lib Ws2_32.lib User32.lib /nologo /dll /machine:I386 /out:"..\..\java\NMMediator.dll"

!ELSEIF  "$(CFG)" == "NMMediator - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "NMMediator___Win32_Debug"
# PROP BASE Intermediate_Dir "NMMediator___Win32_Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "NMMediator___Win32_Debug"
# PROP Intermediate_Dir "NMMediator___Win32_Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "NMMEDIATOR_EXPORTS" /YX /FD /GZ /c
# ADD CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /I "ncp\wpcap" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "NMMEDIATOR_EXPORTS" /D "WPCAP" /D "HAVE_REMOTE" /FD /GZ /c
# SUBTRACT CPP /YX
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x804 /d "_DEBUG"
# ADD RSC /l 0x804 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /dll /debug /machine:I386 /pdbtype:sept
# ADD LINK32 LIBCMT.LIB sqlite3\sqlite3.lib Ws2_32.lib User32.lib ncp\wpcap\Packet.lib ncp\wpcap\wpcap.lib /nologo /dll /debug /machine:I386 /out:"..\..\java\NMMediator.dll" /pdbtype:sept

!ENDIF 

# Begin Target

# Name "NMMediator - Win32 Release"
# Name "NMMediator - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\telnet\ansi.cpp
# End Source File
# Begin Source File

SOURCE=.\telnet\console.cpp
# End Source File
# Begin Source File

SOURCE=.\ncp\grammar\grammar.c
# End Source File
# Begin Source File

SOURCE=.\ncp\grammar\grammar_tab.c
# End Source File
# Begin Source File

SOURCE=.\telnet\helpsock.cpp
# End Source File
# Begin Source File

SOURCE=.\ncp\grammar\lex.yy.c
# End Source File
# Begin Source File

SOURCE=.\md5\md5c.c
# End Source File
# Begin Source File

SOURCE=.\md5\md5class.cpp
# End Source File
# Begin Source File

SOURCE=.\ncp\ncp.cpp
# End Source File
# Begin Source File

SOURCE=.\ncp\ncp_cap.cpp
# End Source File
# Begin Source File

SOURCE=.\ncp\ncp_log.c
# End Source File
# Begin Source File

SOURCE=.\ncp\ncp_parse.cpp
# End Source File
# Begin Source File

SOURCE=.\ncp\ncp_rule.cpp
# End Source File
# Begin Source File

SOURCE=.\NMMediator.cpp
# End Source File
# Begin Source File

SOURCE=.\NMMediator.def
# End Source File
# Begin Source File

SOURCE=.\telnet\nvt.cpp
# End Source File
# Begin Source File

SOURCE=.\TaskScheduler.cpp
# End Source File
# Begin Source File

SOURCE=.\telnet\telnet.cpp
# End Source File
# Begin Source File

SOURCE=.\tinyxml\tinystr.cpp
# End Source File
# Begin Source File

SOURCE=.\tinyxml\tinyxml.cpp
# End Source File
# Begin Source File

SOURCE=.\tinyxml\tinyxmlerror.cpp
# End Source File
# Begin Source File

SOURCE=.\tinyxml\tinyxmlparser.cpp
# End Source File
# Begin Source File

SOURCE=.\telnet\vm.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\telnet\console.h
# End Source File
# Begin Source File

SOURCE=.\md5\global.h
# End Source File
# Begin Source File

SOURCE=.\ncp\grammar\grammar.h
# End Source File
# Begin Source File

SOURCE=.\ncp\grammar\grammar_tab.h
# End Source File
# Begin Source File

SOURCE=.\md5\MD5.h
# End Source File
# Begin Source File

SOURCE=.\md5\md5class.h
# End Source File
# Begin Source File

SOURCE=.\ncp\ncp_cap.h
# End Source File
# Begin Source File

SOURCE=.\ncp\ncp_log.h
# End Source File
# Begin Source File

SOURCE=.\ncp\ncp_parse.h
# End Source File
# Begin Source File

SOURCE=.\ncp\ncp_rule.h
# End Source File
# Begin Source File

SOURCE=.\sqlite3\sqlite3.h
# End Source File
# Begin Source File

SOURCE=.\TaskScheduler.h
# End Source File
# Begin Source File

SOURCE=.\telnet\telnet.h
# End Source File
# Begin Source File

SOURCE=.\telnet\TelnetImpl.h
# End Source File
# Begin Source File

SOURCE=.\tinyxml\tinystr.h
# End Source File
# Begin Source File

SOURCE=.\tinyxml\tinyxml.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# End Group
# End Target
# End Project
