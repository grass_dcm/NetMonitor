#ifndef _APPS_NET_TELNET_H
#define _APPS_NET_TELNET_H
#include <vector>
using namespace std;

enum TELNET_STATUS { eInit=0, eData, eHold, eMessage };

typedef struct _TELNET_META_ {
	string      keyword;
	string      value;
} TELNET_META;

typedef struct _TELNET_PROPS_ {
	int                  port;         /** router port for telnet */
	char                 prompt[32];   /** router returns ehco string */
	vector<TELNET_META>  metaData;     /** to save keyword and realted value */
	TELNET_STATUS        status;       /** current status */
} TELNET_PROPS;

bool telnet_send(const char *buf, int len);
void close_telnet();

extern char const* sockmsg(int ecode);
extern void err(char const* s,...);

extern bool telnet(char const* pszHostName,const short nPort);
extern void vm(SOCKET,unsigned char);

// terminal handlers:
void ansi(SOCKET server,unsigned char data);
void nvt(SOCKET server,unsigned char data);

// helpsock
char const* sockmsg(int ecode);

// command shell
int shell(char const* pszHostName,const int nPort);

#endif /* ndef _APPS_NET_TELNET_H */

