/*#ifdef SNMPDLL_EXPORTS
#define SNMPDLL_API __declspec(dllexport) __stdcall
#else
#define SNMPDLL_API __declspec(dllimport)
#endif
*/

#define SNMPDLL_API __stdcall
#ifdef __cplusplus
extern "C" {
#endif

bool SNMPDLL_API dll_snmp_open(char* mibs_path, char* peername, bool snmpv3, char* security, char* password);
bool SNMPDLL_API dll_snmp_close();
bool SNMPDLL_API dll_snmp_command(char* oids, char type, unsigned char* value, unsigned int value_len);
int  SNMPDLL_API dll_snmp_get_info(char* oids, unsigned char* type, char* value, unsigned int len);

#ifdef __cplusplus
}
#endif