/**
 * NetMonitor Library JNI encapsulation
 * 
 * Copyright (C) 2007 keesion <keesion@gmail.com>
 */

package netmonitor;

/**
 *  任务的属性类，包含了任务的属性信息 
 * @author keesion@gmail.com
 */
public class TaskProps {
	/**
	 * Timer触发的时间间隔，以秒为单位。也就是每隔多久向设备发送commandString所指定的命令
	 */
	public int       frequency = 0;
	
	/**
	 * 任务开始的具体的时间，注意是长整型（表示从1970年1月1日开始的毫秒数）。如果java用Date类来表示时间，要调用getTime()返回<br>
	 * 注意下面的特殊形式：<br>
	 * 1). if specified=0,则打监听开始时(startMonitor)，便按frequency的值开始触发<br>
	 * 2). if specified>0,则specified为第一次触发的开始时间<br>
	 * 3). if frequency=0,为一次性触发。
	 */
	public long      specified = 0;

	/**
	 * 任务的描述信息。　eg. desc="2007-09-01 8:00 封掉来自192.168.1.5的数据包";
	 */
	public String    desc = "";	
	
	/**
	 * 本任务是否生效。如果enable=false,则忽略此任务
	 */
	public boolean   enabled = true;
	
	/**
	 * 采用什么协议向设备发送命令<br>
	 * 1). protocol=0x01, 采用SNMP协议<br>
	 * 2). protocol=0x02, 采用Telnet协议
	 */
	public int      protocol = 0x01;

	/**
	 * 向设备发送的命令。<br>
	 * 此命令为路由器或交换机等设备支持的命令，命令中已加入相应的IP,端口,MAC等参数
	 */
	public String   lockCommand = "";
}
