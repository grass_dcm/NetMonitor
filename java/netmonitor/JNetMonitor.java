/**
 * NetMonitor Library JNI encapsulation
 * 
 * Copyright (C) 2007 keesion <keesion@gmail.com>
 */

package netmonitor;

/**
 * 　调用本地库的JNI封装, NetMonitor库提供的所有接口都可通过此类调用
 */
public class JNetMonitor {
	private IEventObject loggerEventObject;
	
	/* 中间件的名称为 NetMonitor */
	protected static String LIB_NETMONITOR_WRAPPER = "JNetMonitor";
	static {
		System.err.print("Loading native library NetMonitor... ");
		System.loadLibrary(LIB_NETMONITOR_WRAPPER);
		System.err.println("OK");
	}
	
	/**
	 * 打开数据库
	 * @param fileName 数据文件名，为NULL时，以默认文件名(NMConfig.db)打开
	 * @return 0 函数执行成功<br>
	 *        -1 操作失败
	 */
	public native int openDatabase(String fileName);

	/**
	 * 关闭数据库
	 * @return 0 函数执行成功<br>
	 *        -1 操作失败<br>
	 *        -2 数据库尚未打开
	 */
	public native int closeDatabase();

	/**
	 * 判断是否存在记录
	 * @param strSQL 查询的SQL语句
	 * @return true  <=> 存在<br>
	           false <=> 不存在
	 */
	public native boolean hasRecordset(String strSQL);

	/**
	 * 执行一条sql语句
	 * @param strSQL 要执行的sql语句
	 * @return 0 函数执行成功<br>
	 *        -1 操作失败
	 */
	public native int executeSQL(String strSQL);

	/**
	 * 返回最后一条错误信息
	 */	
	public native String getLastError();

	/* 处理规则的API ( Rule API ) */
	/**
	 * 加入一条规则
	 * @param xmlProps xml结构的规则属性信息
	 * @return 对传入数据进行MD5加密后所得的16进制字符串，做为每条规则的唯一标识<br>
	 *         返回NULL的时候说明执行失败
	 */
	public native String insertRule(String xmlProps);

	/**
	 * 更新一条规则
	 * @param ruleMark 由insertRule返回的MD5标识串
	 * @param xmlProps xml结构的规则属性信息
	 * @return  0 函数执行成功<br>
	 *         -1 操作失败
	 */
	public native int updateRule(String ruleMark, String xmlProps);

	/**
	 * 删除一条规则
	 * @param ruleMark 由insertRule返回的MD5标识串
	 * @return  0 函数执行成功<br>
	 *         -1 操作失败
	 */
	public native int removeRule(String ruleMark);

	/**
	 * 根据SQL查询语句获取规则标识列表
	 * @param strSQL 标准SQL查询语句。
	 * @return 由规则的标识串组成的String数组
	 */	
	public native String[] getRuleList(String strSQL);

	/**
	 * 获得指定规则的属性信息
	 * @param ruleMark 由insertRule返回的MD5标识串
	 * @return xml结构的规则属性信息
	 */
	public native String getRuleProps(String ruleMark);


	/* 处理任务的API ( Task API ) */		
	/**
	 * 加入一项任务
	 * @param xmlProps xml结构的任务属性信息
	 * @return 对传入数据进行MD5加密后所得的16进制字符串，做为每项任务的唯一标识
	 *         返回NULL的时候说明执行失败
	 */
	public native String insertTask(String xmlProps);
	
	/**
	 * @param taskMark 由insertTask返回的md5标识串
	 * @param xmlProps xml结构的任务属性信息
	 * @return  0 函数执行成功<br>
	 *         -1 操作失败
	 */
	public native int updateTask(String taskMark, String xmlProps);
	
	/**
	 * 删除一项任务
	 * @param taskMark 由insertTask返回的MD5标识串
	 * @return  0 函数执行成功<br>
	 *         -1 操作失败
	 */
	public native int removeTask(String taskMark);
	
	/**
	 * 根据SQL查询语句获取任务的标识列表
	 * @param strSQL 标准SQL查询语句。
	 * @return 由任务的标识串组成的String数组
	 */
	public native String[] getTaskList(String strSQL);
	
	/**
	 * 获得指定任务的属性信息
	 * @param taskMark 由insertTask返回的MD5标识串
	 * @return xml结构的任务属性信息
	 */
	public native String getTaskProps(String taskMark);
	
	
	/* 处理统计流量的API ( Task API ) */	
	/**
	 * 加入一条统计规则
	 * @param syntax 统计规则的语法
	 * @return 对传入数据进行MD5加密后所得的16进制字符串
	 *         注意：本函数的返回指针不需要释放
	 *         返回NULL的时候说明执行失败
	 */
	public native String insertStats(String syntax);
	
	/**
	 * 更新一条统计规则
	 * @param statsMark 由insertStats返回的md5标识串
	 * @param syntax 统计规则的语法
	 * @return  0 函数执行成功<br>
	 *         -1 操作失败
	 */
	public native int updateStats(String statsMark, String syntax);
	
	/**
	 * 删除一条统计规则
	 * @param statsMark 由insertStats返回的MD5标识串
	 * @return  0 函数执行成功<br>
	 *         -1 操作失败
	 */
	public native int removeStats(String statsMark);
	
	/**
	 * 根据SQL查询语句获取统计规则的标识列表
	 * @param strSQL 标准SQL查询语句。
	 * @return 由统计规则的标识串组成的String数组
	 */
	public native String[] getStatsList(String strSQL);
	
	/**
	 * 获得指定统计规则的属性信息
	 * @param statsMark   由insertStats返回的MD5标识串
	 * @return  统计规则的语法
	 */
	public native String getStatsProps(String statsMark);	
	
	
	/* 混合型API ( Miscellaneous API ) */
	
	/**
	 * 设置用什么协议发送系统的自动命令
	 * @param protocol 发送的协议<br>
	 *                 1). protocol=0x01, 用SSH协议发送<br>
	 *                 2). protocol=0x02, 用Telnet协议发送
	 * @return  0 函数执行成功<br>
	 *         -1 操作失败
	 */
	public native int setAutoCommandProtocol(int protocol);
	
	/**
	 * 给设备发送一条指令
	 * @param protocol 发送的协议<br>
	 *                 1). protocol=0x01, 用SSH协议发送<br>
	 *                 2). protocol=0x02, 用Telnet协议发送
	 * @param commandString 设备支持的命令字符串
	 * @return  0 函数执行成功<br>
	 *         -1 操作失败
	 */
	public native int sendCommandToDevice(int protocol, String commandString);

	/**
	 * 读取本地的系统网络数据流量
	 * @param statsMark 统计规则的md5标识串
	 * @return 数据流量，单位为byte
	 */
	public native long getLocalIOBytes(String statsMark);

	/**
	 * 读取本地的系统网络数据包个数
	 * @param statsMark 统计规则的md5标识串
	 * @return 数据包个数
	 */
	public native long getLocalIOPackets(String statsMark);

	/**
	 * 返回所有网卡的名称
	 * @return 网卡名称列表
	 */	
	public native String[] lookupDevices();
	
	/**
	 * 启动网络数据监控
	 * @param device 网卡的名称(friendly name)
	 * @param snaplen 要监控的数据包总长度, 满足长度后停止监控
	 * @param promiscuouse 是否要混合监听模式
	 * @param timeout 监控的时间，为0时，不限时
	 * @param filter  过滤语法
	 * @return 0 函数执行成功
	 */
	public native int startMonitor(String device, int snaplen, 
			boolean promiscuouse, int timeout, String filter);
	
	/**
	 * 停止网络数据监控
	 * @return 0 函数执行成功
	 */
	public native int stopMonitor();
	
	/**
	 * 设置远程设备ip
	 * @param ip 远程设备ip
	 * @return 0 函数执行成功<br>
	 *        -1 操作失败
	 */	
	public native int setRemoteIp(String ip);
	
	/**
	 * 获取远程设备ip
	 * @return 远程设备ip
	 */	
	public native String getRemoteIp();
	
	/**
	 * 设置Telnet配置信息
	 * @param xmlProps Telnet的XML配置信息
	 * @return 0 函数执行成功<br>
	 *        -1 操作失败
	 */
	public native int setTelnetConfig(String xmlProps);
	
	/**
	 * 获取Telnet配置信息
	 * @return 返回Telnet的XML配置信息
	 */
	public native String getTelnetConfig();
	
	/**
	 * 设置SNMP配置信息
	 * @param version 版本号
	 * @param mibspath mib存放路径
	 * @param security 如果是V1就是PUBLIC，如果是V3就是用户名
	 * @param password v3, 用户密码
	 * @return 0 函数执行成功<br>
	 *        -1 操作失败
	 */	
	public native int setSnmpConfig(int version, String mibspath, String security, String password);

	/**
	 * 获取SNMP配置信息
	 * @param config SNMP相关配置参数
	 * @return 0 函数执行成功<br>
	 *        -1 操作失败
	 */
	public native int getSnmpConfig(SnmpConfig config);
	
	/**
	 * 与SNMP Server连接
	 * @return 0 函数执行成功<br>
	 *        -1 操作失败
	 */
	public native int openSnmpServer();
	
	/**
	 * 根据oids设置路由中的对应值
	 * @param oids  MIB OID,形式".1.3.6.1.2.1.1.5.1.0"
	 * @param type  值的类型,比如0x04为字符串类型
	 * @param value 值的二进制数据，以byte数组存放
	 * @return 0 函数执行成功<br>
	 *        -1 操作失败
	 */
	public native int sendSnmpObjectMeta(String oids, int type, byte[] value);
	
	/**
	 * 根据oids获取路由中的表值
	 * @param metaObj SNMP返回指定mib oid对象的数据
	 * @return 0 函数执行成功<br>
	 *        -1 操作失败
	 */
	public native int getSnmpObjectMeta(SnmpObjectMeta metaObj);
	
	/**
	 * 关闭与SNMP Server的连接
	 * @return 0 函数执行成功<br>
	 *        -1 操作失败
	 */
	public native int closeSnmpServer();
	
	/**
	 * 设置SSH配置信息
	 * @param username 用户名
	 * @param password 密码
	 * @param port SSH Server端口号
	 * @param key SSH Private Key
	 * @return 0 函数执行成功<br>
	 *        -1 操作失败
	 */	
	public native int setSSHConfig(String username, String password, int port, String key);
	
	/**
	 * 获取SSH配置信息
	 * @param config SSH相关配置参数
	 * @return 0 函数执行成功<br>
	 *        -1 操作失败
	 */	
	public native int getSSHConfig(SSHConfig config);
	
	/**
	 * 设置是否对数据包进行 ip<=>mac 的提取
	 * @param bEnabled true   开始提取<br>
	 *                 false  停止提取
	 * @return 0 函数执行成功<br>
	 *        -1 操作失败
	 */
	public native int setMapEnabled(boolean bEnabled);
	/**
	 * 获取通过Device上的IP映射表
	 * @return Device上的IP映射表
	 */
	public native String[] getIpsMapTable();
	
	/**
	 * 根据ip取得对应的mac
	 * @param ip ip字串，由getIpsMapTable返回的列表中挑选
	 * @return　mac
	 */
	public native String getMacByIp(String ip);
	
	/**
	 * 清除ip映射表，释放占用的内存
	 * @return 0 函数执行成功<br>
	 *        -1 操作失败
	 */
	public native int clearIpsMapTable();

	/**
	 * 设置警告或日志事件对象
	 * @param eventObject 继承自IEventObject的事件对象
	 */
	public void setEventCallback(IEventObject eventObject)
	{
		loggerEventObject = eventObject;
	}
	
	public void doEvent(int comefrom, String md5Mark, String desc, RuleIPPacket packet)
	{
		loggerEventObject.fireEvent(comefrom, md5Mark, desc, packet);
	}
}
