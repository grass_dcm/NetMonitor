/**
 * NetMonitor Library JNI encapsulation
 * 
 * Copyright (C) 2007 keesion <keesion@gmail.com>
 */

package netmonitor;

/**
 * 规则的属性类，包含了规则的属性信息 
 * @author keesion@gmail.com
 */
public class RuleProps{
	
	/**
	 * 规则的语法文本
	 */
	public String    ruleSyntax;
	
	/**
	 * 规则的描述信息。　eg. desc="QQ数据包";
	 */
	public String    desc = "";
	
	/**
	 * 规则列表是由很多规则组成的。所以在处理的时候可以指定优先级，priority越小，赿优先处理本条规则
	 */
	public int       priority = 0;
	
	/**
	 * 本条规则是否生效。如果enabled=false,则处理数据包的时候不对本条规则进行匹配
	 */
	public boolean   enabled = true;
	
	/**
	 * 封锁指令的超时时间<br>
	 * 当检测到有匹配此条规则的数据包时，系统会向设备发送一条封锁指令<br>
	 * 当过了timeout的时间，系统还会再发送一条对应的解封指令
	 */
	public int       timeout = 0;
	
	/**
	 * 锁定命令
	 */
	public String    lockCommand = "";
	
	/**
	 * 解锁命令
	 */
	public String    unlockCommand = "";
	
}
