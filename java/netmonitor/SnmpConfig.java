package netmonitor;

public class SnmpConfig {
	/** 版本号 */
	public int    version = 1;
	
	/** mib存放路径 */
	public String mibspath = "";
	
	/** 如果是V1就是PUBLIC，如果是V3就是用户名 */
	public String security = "";
	
	/** v3, 用户密码 */
	public String password = "";
}
