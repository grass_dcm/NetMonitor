package netmonitor;

public class SnmpObjectMeta {
	/** 传入参数， oids */
	public String      oids = "";
	
	/** 返回数据的类型 */
	public int         type = 0;
	
	/** 返回数据的byte数组 */
	public byte[]      value;
}
