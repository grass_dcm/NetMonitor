/**
 * NetMonitor Library JNI encapsulation
 * 
 * Copyright (C) 2007 keesion <keesion@gmail.com>
 */

package netmonitor;

/**
 * 警告或日志的事件接口，准备对事件进行具体处理的类要继承此接口，实现fireEvent
 * @author keesion@gmail.com
 */
public interface IEventObject {
	
	/**
	 * 触发事件
	 * @param comefrom 事件类别<br>
	 *                 1). comefrom=0x01 来自规则自动触发事件<br>
	 *                 2). comefrom=0x02 来自任务触发事件
	 * @param md5Mark 规则或任务的MD5标识串<br>
	 *                 1). if comefrom=0x01 md5Mark为规则的标识串<br>
	 *                 2). if comefrom=0x02 md5Mark为任务的标识串
	 * @param desc 事件的描述，做为保留参数，不见得会用到。
	 */
	public void fireEvent(int comefrom, String md5Mark, String desc, RuleIPPacket packet);
}
