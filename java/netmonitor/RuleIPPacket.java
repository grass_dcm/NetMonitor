package netmonitor;

public class RuleIPPacket {
	
	/** IP数据包的信息 */
	public class PacketData {
		/** MAC地址 */
		public String mac;

		/** IP地址 */
		public String ip;

		/** 端口号 */
		public int port;
	}


	/** IP 类型 (TCP=6 or UDP=17) */
	public int type;

	/** 进入的数据包 */
	PacketData src;

	/** 出去的数据包*/
	PacketData des;
}


