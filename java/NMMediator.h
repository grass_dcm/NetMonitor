/**
 * NMMediator Library
 * 
 * Copyright (C) 2007 keesion <keesion@gmail.com>
 */

#ifndef __NM_MEDIATOR_H
#define __NM_MEDIATOR_H

/**
 * @file NMMediator.h
 * @brief NMMeidator standard header, incuding data structs and APIs.
 */

/**
 * Structure for Rule Properties metadata
 * @struct RulePropsMeta
 */
typedef struct _RULE_PROPS_META_ {
    /**
     * 规则的语法
     */
    char        *ruleSyntax;
//    char        *data;
//    int         datalen;

    /**
     * 规则的描述信息。　eg. desc="QQ数据包";
     */
    char        *desc;

    /**
     * 规则列表是由很多规则组成的。所以在处理的时候可以指定优先级，priority越小，赿优先处理本条规则
     */
    int         priority;

    /**
     * 本条规则是否生效。如果enabled=false,则处理数据包的时候不对本条规则进行匹配
     */
    bool        enabled;

    /**
     * 封锁指令的超时时间<br>
     * 当检测到有匹配此条规则的数据包时，系统会向设备发送一条封锁指令<br>
     * 当过了timeout的时间，系统还会再发送一条对应的解封指令
     */
    int         timeout;

} RulePropsMeta;


typedef struct _EVENT_OBJECT_ {
    /**
     * 事件类别<br>
     *      1). comefrom=0x01 来自规则自动触发事件<br>
     *      2). comefrom=0x02 来自任务触发事件
     */
    int         comefrom;

    /**
     * 规则或任务的MD5标识串<br>
     *      1). if comefrom=0x01 md5Mark为规则的标识串<br>
     *      2). if comefrom=0x02 md5Mark为任务的标识串
     */
    char        *md5Mark;

    /**
     * 事件的描述，做为保留参数，不见得会用到。
     */
    char        *desc;

} EventObject;


#define STDCALL __stdcall
#ifdef __cplusplus
extern "C" {
#endif

/**
 * 事件的回调函数接口定义
 */
typedef void (STDCALL *EventCallback)(EventObject eventObj);

#define NMS_SUCCESS  0
#define NMS_FAILED  -1
extern EventCallback fireLoggerEventCallbak;

/**
 * 加入一条规则
 * @param xmlProps xml结构的规则属性信息
 * @return 对传入数据进行MD5加密后所得的16进制字符串，做为每条规则的唯一标识<br>
 *         注意：本函数的返回指针不需要释放 
 *         返回NULL的时候说明执行失败
 */
char *  STDCALL NM_insertRule(const char *xmlProps);
/**
 * 更新一条规则
 * @param ruleMark 由insertRule返回的MD5标识串
 * @param xmlProps xml结构的规则属性信息
 * @return  0 函数执行成功<br>
 *         -1 操作失败
 */
int     STDCALL NM_updateRule(const char *ruleMark, const char *xmlProps);
/**
 * 删除一条规则
 * @param ruleMark 由insertRule返回的MD5标识串
 * @return  0 函数执行成功<br>
 *         -1 操作失败
 */
int     STDCALL NM_removeRule(const char *ruleMark);
/**
 * 根据SQL查询语句获取规则标识列表
 * @param strSQL 标准SQL查询语句。
 * @param count  返回规则的数量
 * @return 由规则的标识串组成的String数组<br>
 *         用完后，用NM_freeList释放内存
 */
char**  STDCALL NM_getRuleList(const char *sql, int *count);
/**
 * 获得指定规则的属性信息
 * @param ruleMark   由insertRule返回的MD5标识串
 * @return  xml结构的规则属性信息
 */
char *  STDCALL NM_getRuleProps(const char *ruleMark);

/**
 * 加入一项任务
 * @param xmlProps xml结构的任务属性信息
 * @return 对传入数据进行MD5加密后所得的16进制字符串，做为每项任务的唯一标识
 *         注意：本函数的返回指针不需要释放
 *         返回NULL的时候说明执行失败
 */
char *  STDCALL NM_insertTask(const char *xmlProps);
/**
 * @param taskMark 由insertTask返回的md5标识串
 * @param xmlProps xml结构的任务属性信息
 * @return  0 函数执行成功<br>
 *         -1 操作失败
 */
int     STDCALL NM_updateTask(const char *taskMark, const char *xmlProps);
/**
 * 删除一项任务
 * @param taskMark 由insertTask返回的MD5标识串
 * @return  0 函数执行成功<br>
 *         -1 操作失败
 */
int     STDCALL NM_removeTask(const char *taskMark);
/**
 * 根据SQL查询语句获取任务的标识列表
 * @param strSQL 标准SQL查询语句。
 * @param count  返回规则的数量
 * @return 由任务的标识串组成的String数组
 *         用完后，用NM_freeList释放内存
 */
char**  STDCALL NM_getTaskList(const char *sql, int *count);
/**
 * 获得指定任务的属性信息
 * @param taskMark   由insertTask返回的MD5标识串
 * @return  xml结构的任务属性信息
 */
char *  STDCALL NM_getTaskProps(const char *taskMark);

/**
 * 给设备发送一条指令
 * @param protocol 发送的协议<br>
 *                 1). protocol=0x01, 用SNMP协议发送<br>
 *                 2). protocol=0x02, 用Telnet协议发送
 * @param commandString 设备支持的命令字符串
 * @return  0 函数执行成功<br>
 *         -1 操作失败
 */
int     STDCALL NM_sendCommandToDevice(int protocol, const char *commandString);
/**
 * 读取本地的系统网络数据流量
 * @param ruleMark 规则的md5标识串
 * @return 数据流量，单位为byte
 */
long    STDCALL NM_getLocalIOBytes(const char *ruleMark);
/**
 * 设置警告或日志事件对象
 * @param EventCallback function prototype
 * @return NULL(void)
 */
void    STDCALL NM_setEventCallback(EventCallback fireEventObj);
/**
 * 启动网络数据监控
 * @param device 网卡的名称(friendly name)
 * @param snaplen 要监控的数据包总长度, 满足长度后停止监控
 * @param promiscuous 是否要混合监听模式
 * @param timeout 监控的时间，为0时，不限时
 * @param filter  过滤语法
 * @return 0 函数执行成功
 */
int     STDCALL NM_startMonitor(const char* device, int snaplen, 
                                bool promiscuous, int timeout, 
                                const char *filter);
/**
 * 停止网络数据监控
 * @return 0 函数执行成功
 */
int     STDCALL NM_stopMonitor();

/**
 * 分配内存
 * @param size 要分配的内存的大小
 */
void*   STDCALL NM_malloc(int size);
/**
 * 释放内存
 * @param pObject 要释要的内存地址
 */
void    STDCALL NM_free(void *pObject);

/**
 * 释放列表内存
 * @param ppObject 列表首地址
 * @parm  count    字串列表中字串的数量
 */
void    STDCALL NM_freeList(char **ppObject, int count);

/**
 * 打开数据库
 * @param fileName 数据文件名，为NULL时，以默认文件名(NMConfig.db)打开
 * @return 0 函数执行成功<br>
 *        -1 操作失败
 */
int     STDCALL NM_openDatabase(char *fileName);
/**
 * 关闭数据库
 * @return 0 函数执行成功<br>
 *        -1 操作失败<br>
 *        -2 数据库尚未打开
 */
int     STDCALL NM_closeDatabase();
/**
 * 判断是否存在记录
 * @param sql 查询的SQL语句
 * @return true  <=> 存在<br>
           false <=> 不存在
 */
bool    STDCALL NM_hasRecordset(const char *sql);
/**
 * 返回最后一条错误信息
 */
const char*   STDCALL NM_getLastError();

/**
 * 返回所有网卡的名称
 * @return 网卡名称列表
 */
char **  STDCALL NM_lookupDevices();

//int      STDCALL NM_setFilter(const char *filter, bool optimize);

#ifdef __cplusplus
}
#endif
#endif



