
import java.io.IOException;

import netmonitor.*;

public class MainTest {

	public static void main(String[] args) throws IOException, InterruptedException {
		MainTest TestCase = new MainTest();
		JNetMonitor netMonitor = new JNetMonitor();
		//必须首先调用openDatabase
		netMonitor.openDatabase(null);
		
		// 测试用例
		TestCase.test_execute_sql(netMonitor);		
//		TestCase.test_snmp(netMonitor);
		TestCase.test_rule(netMonitor);
//		TestCase.test_task(netMonitor);
//		TestCase.test_telnet_config(netMonitor);

		String[] devices = netMonitor.lookupDevices();
		for (int i=0; i<devices.length; i++) {
			System.out.printf("%d, %s\n", i+1, devices[i]);
		}
		System.out.print("请选择网卡号:");	
		char input = (char) System.in.read();
		int select = Integer.parseInt(String.valueOf(input));
		if ( select<1 || select>devices.length )
		{
			System.out.println("网卡号选择错误");
			System.exit(0);
		}
		
		//设置事件回调对象
		LoggerEvent eventObject = new LoggerEvent();
		netMonitor.setEventCallback(eventObject);
		
		netMonitor.startMonitor(devices[select-1], 10240, true, 100, null);
		//准备开始取得IP=>MAC映射表, 必须在startMontior后调用
//		netMonitor.setMapEnabled(true);
		
		while ( true )
		{
			// 控制台输入q 回车，退出
			input = (char) System.in.read();
			
			if ( input=='q' )
			{
				netMonitor.stopMonitor();
				netMonitor.closeDatabase();
				break;
			}
			//Console下输入m, 测试取得IP=>MAC映射表
/*			else if ( input=='m' )
			{
				String[] ipTable = netMonitor.getIpsMapTable();
				for ( int i=0; i<ipTable.length; i++ )
				{
					String macString = netMonitor.getMacByIp(ipTable[i]);
					System.out.println(macString);
				}
			}*/
			//Console下输入c, 清除IP=>MAC映射表, 并停止映射
			else if ( input=='c' )
			{
				netMonitor.clearIpsMapTable();
				netMonitor.setMapEnabled(false);
			}
			else if ( input=='k' )
			{
				TestCase.test_stats(netMonitor);
			}
		}
		
		System.exit(0);
	}
	
	/*
	 * 测试用例
	 */
	//snmp测试
	void test_snmp(JNetMonitor netMonitor)
	{
		SnmpObjectMeta metaObj = new SnmpObjectMeta();
		netMonitor.openSnmpServer();
		String metaData = "yy";
		netMonitor.sendSnmpObjectMeta(".1.3.6.1.2.1.1.6.0", 0x04, metaData.getBytes());
		
		metaObj.oids = ".1.3.6.1.2.1.1.6.0";
		netMonitor.getSnmpObjectMeta(metaObj);
		String value = new String(metaObj.value);
		System.out.println(value);
		netMonitor.closeSnmpServer();	
	}
	
	//telnet信息配置测试
	void test_telnet_config(JNetMonitor netMonitor)
	{
		

	}

	//ssh
	void test_ssh_config(JNetMonitor netMonitor)
	{
		SSHConfig config = new SSHConfig();
		
		config.username = "administrator";
		config.password = "123456";
		netMonitor.setSSHConfig(config.username, config.password, config.port, "");
		
		config.username = "";
		config.password = "";
		config.port = 0;
		netMonitor.getSSHConfig(config);
	}
	
	void test_execute_sql(JNetMonitor netMonitor)
	{
		netMonitor.executeSQL("delete from RuleTable");
		netMonitor.executeSQL("delete from TaskTable");
		netMonitor.executeSQL("delete from StatsTable");
	//	netMonitor.executeSQL("update ConfigTable set telnet_port=23");		
	}
	
	// 规则配置测试
	void test_rule(JNetMonitor netMonitor)
	{
	    String xmlProps = "<ruleprops>\n" +
			"<syntax>packet.data[12-13]='0800' and packet.data[23]=1 end</syntax>\n" +
		        "<desc>Filter QQ Application</desc>\n" +
		        "<enabled>true</enabled>\n" +
		        "<timeout>100</timeout>\n" +
		        
		        "<command>\n" +
		        "    <lock>access-list 102 deny tcp any any range 6881 6890</lock>\n" +
		        "    <unlock>access-list 102 permit ip</unlock>\n" +
		        "</command>\n" +
		    "</ruleprops>";
	    
	    netMonitor.insertRule(xmlProps);
	}
	
	// 任务配置测试
	void test_task(JNetMonitor netMonitor)
	{
		for (int i=1; i<51; i=i+5)
		{
			String xmlProps = String.format(
				"<taskprops>\n" + 
				"<specified>%d</specified>\n" + 
				"<interval>5</interval>\n" +
				"<frequency>5</frequency>\n" +
				"<command>access-list 102 permit ip</command>\n" +
				"<desc>Trigger per 1000ms</desc>\n" +
				"</taskprops>", i);
			
			netMonitor.insertTask(xmlProps);
		}
		
		String []taskList = netMonitor.getTaskList("select * from TaskTable");
		for (int i=0; i<taskList.length; i++)
		{
			String xmlProps = netMonitor.getTaskProps(taskList[i]);
			System.out.println(xmlProps);
		}
	}
	
	// 统计测试
	void test_stats(JNetMonitor netMonitor)
	{
		String syntax = "ip.des=10.193.155.185 and packet.data[12-13]='0800' and packet.data[23]=1 end";
		String md5String = netMonitor.insertStats(syntax);
		try{Thread.sleep(10000);}catch(Exception e){}
		long localBytes = netMonitor.getLocalIOBytes(md5String);
		System.out.printf("==================== [%d]\n", localBytes);
		netMonitor.removeStats(md5String);
	}
}
