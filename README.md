### NetMonitor 简介

可监控用户的上网行为，通过自定义的语法，设定规则或软件特征码，如果发现用户上QQ,MSN,eMule等应用，可以通过snmp、telnet或ssh控制路由器，封掉其IP或相关端口。

从putty代码中，提取了SSH和Telnet的封装，还利用了SQLite3，winpcap，net-snmp，log4cplus等第三方开源库。

很早的项目，07年初完成的，本代码**仅供学习参考**，严禁用于商业项目，也不提供技术支持，维护等，所以请勿咨询。

### 架构图

![架构图](http://git.oschina.net/zencodex/NetMonitor/raw/master/NetMonitor/doc/arch.jpg)

- NMMediator.dll：核心组件，标准的动态库，支持任何语言的调用。
- JNetMonitor.dll：为JAVA程序封装的JNI，方便JAVA的调用。

### 第三方开源库

winpcap：主要用于侦听网络底层数据包
SQLite3：用于本地数据的存储
net-snmp：封装了v1-v3的snmp协议
log4cplus：用于生成调试log

### 语法规则模块

定义数据过滤的规则，如果规则匹配上了，则会采用相应的协议(SSH/Telnet)向设备发送封锁指令， 并向任务列表中添加一条对应的解锁指令。每条指令都有一个超时时间，到超时时间后，任务模块 会再向设备发送一条解锁指令。

规则语法定义如下：

	<ruleprops>
			<syntax>tcp.port.src=80 end</syntax>
			<desc>Filter QQ Application</desc>
			<enabled>true</enabled>
			<timeout>100</timeout>
			<command>
					<lock>access-list 102 deny tcp any any range 6881 6890</lock>
					<unlock>access-list 102 permit ip</unlock>
			</command>
	</ruleprops>

### 统计模块

系统会根据此处设置的统计规则对数据流量进行统计。统计规则也同样采用规则语法。比如：

	tcp.port.src=80 end

### 任务模块

任务模块是一个定时系统，每个加入任务列表项都将成为被监控对象。当时间事件触发后，会发送一条预设指令，并产生log事件。

	<taskprops>
			<specified>6</specified>
			<interval>5</interval>
			<frequency>5</frequency>
			<command>access-list 102 permit ip</command>
			<desc>Trigger per 1000ms</desc>
	</taskprops>

### 协议模块

本系统支持3种远程控制协议，分别如下：

- SSH协议
- Telnet协议
- SNMP协议

